TEMPLATE = app
TARGET = TheGame

QT += core gui widgets

CONFIG += c++14

SOURCES += \
    main.cpp \
    mapwindow.cc \
    objectmanager.cc \
    gameeventhandler.cc \
    settingswindow.cc \
    til/forest.cpp \
    til/grassland.cpp \
    graphics/gamescene.cpp \
    graphics/mapitem.cpp \
    til/mountain.cpp \
    til/rocks.cpp \
    til/water.cpp \
    troops/spearman.cpp \
    troops/cavalry.cpp \
    troops/archer.cpp \
    buildings/mine.cpp \
    buildings/quarry.cpp \
    buildings/sawmill.cpp \
    til/field.cpp \
    logic/playerC.cpp \
    troops/worker.cpp \
    buildings/hq.cpp \
    buildings/outpost.cpp \
    buildings/farm.cpp \
    logic/utilities.cpp

HEADERS += \
    mapwindow.hh \
    objectmanager.h \
    settingswindow.h \
    gameeventhandler.h \
    til/forest.h \
    til/grassland.h \
    graphics/gamescene.h \
    graphics/mapitem.h \
    til/mountain.h \
    core_st/resourcemaps.h \
    core_st/combatmaps.h \
    til/rocks.h \
    til/water.h \
    troops/spearman.h \
    troops/cavalry.h \
    troops/archer.h \
    buildings/mine.h \
    buildings/quarry.h \
    buildings/sawmill.h \
    til/field.h \
    data_struct.h \
    logic/playerC.h \
    troops/worker.h \
    buildings/hq.h \
    buildings/outpost.h \
    buildings/farm.h \
    logic/utilities.h


win32:CONFIG(release, debug|release): LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/release/ -lCourseLib
else:win32:CONFIG(debug, debug|release): LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/debug/ -lCourseLib
else:unix: LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/ -lCourseLib

INCLUDEPATH += \
    $$PWD/../Course/CourseLib

DEPENDPATH += \
    $$PWD/../Course/CourseLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/release/libCourseLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/debug/libCourseLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/release/CourseLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/debug/CourseLib.lib
else:unix: PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/libCourseLib.a

FORMS += \
    mapwindow.ui \
    settingswindow.ui

RESOURCES += \
    imgs/resources.qrc
