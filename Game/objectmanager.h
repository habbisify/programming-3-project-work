#ifndef OBJECTMANAGER_H
#define OBJECTMANAGER_H


#include "interfaces/iobjectmanager.h"
#include "buildings/buildingbase.h"
#include "workers/workerbase.h"
#include "logic/playerC.h"

#include "tiles/tilebase.h"
#include "core/playerbase.h"
#include "exceptions/illegalaction.h"

#include "data_struct.h"

#include <map>


namespace Student {
using Player = std::shared_ptr<Student::PlayerC>;

class ObjectManager: public Course::iObjectManager
{

public:
    ObjectManager();

    /**
     * @brief Adds new tiles to the ObjectManager.
     * @param tiles a vector that contains the Tiles to be added.
     * @post The tile-pointers in the vector are stored in the ObjectManager.
     * Exception Guarantee: Basic
     *
     */
    void addTiles(
            const std::vector<std::shared_ptr<Course::TileBase>>& tiles) override;

    /**
     * @brief Returns a shared pointer to a Tile that has specified coordinate.
     * @param coordinate Requested Tile's Coordinate
     * @return a pointer to a Tile that has the given coordinate.
     * If no for the coordinate exists, return empty pointer.
     * @post Exception Guarantee: Basic
     */
    std::shared_ptr<Course::TileBase> getTile(
            const Course::Coordinate& coordinate) override;

    /**
     * @brief Returns a shared pointer to a Tile that has specified ID
     * @param id Tile's ObjectId.
     * @return a pointer to the Tile that has the given ID
     * If no for the id exists, return empty pointer.
     * @post Exception Guarantee: Basic
     */
    std::shared_ptr<Course::TileBase> getTile(const Course::ObjectId& id) override;

    /**
     * @brief Returns a vector of shared pointers to Tiles specified by
     * a vector of Coordinates.
     * @param coordinates a vector of Coordinates for the requested Tiles
     * @return Vector of that contains pointers to Tile's that match
     * the coordinates. The vector is empty if no matches were made.
     * @post Exception Guarantee: Basic
     */
    std::vector<std::shared_ptr<Course::TileBase>> getTiles(
            const std::vector<Course::Coordinate>& coordinates) override;

    /**
     * @brief Add players specified in game_conf_
     */
    void addPlayers();
    Player getPlayer(unsigned int player_index);
    unsigned int getPlayerCount();

    bool setGameConf(data_struct ds);

    std::vector<int> getMapWeights();
    int getSeed();

    bool addBuilding(std::shared_ptr<Course::BuildingBase> building, std::shared_ptr<Course::TileBase> tile, unsigned int p_i);
    bool removeBuilding(Course::ObjectId);

    bool addUnit(std::shared_ptr<Course::WorkerBase> worker, std::shared_ptr<Course::TileBase> tile, unsigned int player_index = 0);


private:
    std::map<Course::Coordinate, std::shared_ptr<Course::TileBase>> map_;
    std::map<Course::ObjectId, std::shared_ptr<Course::TileBase>> map_by_id_;

    // Contains all information about the settings user chose in the Settings-menu
    data_struct game_conf_;

    unsigned int MAX_PLAYERS_ = 2;
    std::vector<Player> players_ = {};
    std::vector<std::shared_ptr<Course::BuildingBase>> buildings_ = {};
    std::vector<std::shared_ptr<Course::WorkerBase>> workers_ = {};
};

} // namespace Student


#endif // OBJECTMANAGER_H
