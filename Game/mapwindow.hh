#ifndef MAPWINDOW_HH
#define MAPWINDOW_HH

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

#include <map>
#include <random>

#include "core/worldgenerator.h"

#include "gameeventhandler.h"
#include "objectmanager.h"
#include "logic/utilities.h"
#include "graphics/gamescene.h"
#include "data_struct.h"
#include "graphics/gamescene.h"

#include "ui_mapwindow.h"

#include "core/gameobject.h"
#include "graphics/simplemapitem.h"

#include "til/forest.h"
#include "til/grassland.h"
#include "til/field.h"
#include "til/mountain.h"
#include "til/rocks.h"
#include "til/water.h"

#include "objectmanager.h"
#include "gameeventhandler.h"
#include "graphics/mapitem.h"
#include "core_st/resourcemaps.h"

#include "buildings/hq.h"
#include "buildings/farm.h"
#include "buildings/outpost.h"
#include "buildings/mine.h"
#include "buildings/quarry.h"
#include "buildings/sawmill.h"

#include "troops/worker.h"
#include "troops/archer.h"
#include "troops/cavalry.h"
#include "troops/spearman.h"

#include "core/playerbase.h"
#include "logic/playerC.h"

#include "exceptions/invalidpointer.h"

#include <math.h>
#include <iostream>
#include <QTableWidgetItem>

#include <QDebug>

namespace Ui {
class MapWindow;
}

class MapWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MapWindow(QWidget *parent = 0);
    ~MapWindow();

    // set the local game event handler variable

    void setGEHandler(std::shared_ptr<Student::GameEventHandler> nHandler);

    // set window size
    void setSize(int width, int height);
    // set window scale
    void setScale(int scale);
    // resize window
    void resize();

    // draws an item to the window
    void drawItem( std::shared_ptr<Course::TileBase> obj);

    // removes and item from the window
    void removeItem( std::shared_ptr<Course::TileBase> obj);

    // updates an item in the window
    void updateItem( std::shared_ptr<Course::TileBase> obj);

    // creates the map with measurements specified in variables x and y
    void drawMap(unsigned int x, unsigned int y);

    // constructs the map's prevalence of different tiles
    void constructMap(Course::WorldGenerator *gen);

    // initializes players
    void initializeGame();

    // initializes player hqs
    void initializePlayerHQ(unsigned int p_i);

    // initializes player workers
    void initializePlayerWorker(unsigned int p_i);

    // Returns the function name string of the latest button element pressed or empty string based on game logic
    std::string getActiveElem();

    Course::Coordinate getCoordinateForHQBuilding(unsigned int p_i);
    Course::Coordinate getCoordinateForBuilding(Course::Coordinate c, unsigned int p_i);
    Course::Coordinate getCoordinateForUnit(Course::Coordinate c, unsigned int p_i);
    bool checkIfValidCoordinateForBuilding(Course::Coordinate c, unsigned int p_i);
    bool checkIfValidCoordinateForUnit(Course::Coordinate c, unsigned int p_i);

    bool buildSelectedBuilding(Course::Coordinate coord);
    bool buildSelectedUnit(Course::Coordinate coord);

    // Helper function that constructs message for Info area
    std::string uiTextResources(std::string s, Student::ResourceMap cost_map);

    bool payForUnit(std::shared_ptr<Course::WorkerBase> unit);

    // returnPaymentUnit is called when player has initiated an illegal training of a unit with enough resources
    void returnPaymentUnit(std::shared_ptr<Course::WorkerBase> unit);

    bool payForBuilding(std::shared_ptr<Course::BuildingBase> building);

    // returnPaymentBuilding is called when player has initiated an illegal construction of a building with enough resources
    void returnPaymentBuilding(std::shared_ptr<Course::BuildingBase> building);

    std::shared_ptr<Course::BuildingBase> returnBuildingType();
    std::shared_ptr<Course::WorkerBase> returnUnitType();

    //std::shared_ptr<Course::WorkerBase> moveSelectedUnit();
    bool moveSelectedUnit(unsigned int p_i);

    // Battle mechanics occur here. The return value is true if the attacker won, and false if the attacker lost
    bool calculateBattleResults(std::shared_ptr<Course::WorkerBase> unit, std::shared_ptr<Course::TileBase> tile);

    // All resource gathering happens here
    void updateResourceMaps(unsigned int p_i);

    // Updates the UI message about player in turn
    void updateTurnLabel();

    // Updates shop's items to correspond the player_in_turn color.
    void updateIcons();
public slots:
    void import_data(data_struct ds);

private slots:
    void on_endTurnB_clicked();

    void on_quitB_clicked();

    void on_workerB_clicked();

    void on_HQB_clicked();

    void on_spearmanB_clicked();

    void on_archerB_clicked();

    void on_cavalryB_clicked();

    void on_outpostB_clicked();

    void on_farmB_clicked();

    void on_mineB_clicked();

    void on_quarryB_clicked();

    void on_sawmillB_clicked();

    void on_select_execB_clicked();

private:
    Ui::MapWindow* m_ui;
    std::shared_ptr<Student::GameEventHandler> m_GEHandler = nullptr;
    std::shared_ptr<Student::ObjectManager> m_ObjManager = nullptr;
    std::shared_ptr<Student::GameScene> m_simplescene = nullptr;
    std::mt19937 m_rnd_engine;
    std::uniform_real_distribution<double> m_rnd_dist;
    QGraphicsScene *gameScene_;

    unsigned int turn_ = 1;
    unsigned int player_index_ = 0;

    // -1 means unresolved winning state, integers 0,1,2,3 signifies player win of corresponding id
    int game_win_ = -1;

    // Last map tile clicked before clicking the execute button (1st time) on its numerical format
    unsigned int map_item_selected_ = 0;

    // Last map tile clicked before clicking the execute button (2nd time) on its numerical format
    unsigned int map_item_exec_ = 0;

    // Contains the function name string of the latest button element pressed or empty string based on game logic
    std::string active_element_ = "";
};

#endif // MapWINDOW_HH

