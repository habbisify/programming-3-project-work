#include "settingswindow.h"

SettingsWindow::SettingsWindow(QWidget *parent) :
    QTabWidget(parent),
    ui(new Ui::SettingsWindow)
{
    ui->setupUi(this);

    ui->p3name->hide();
    ui->p4name->hide();

    this->set_player_names();
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::update_UI(int val)
{
    if (val < 1) {
        ui->p1name->hide();
    } else if (val < 2) {
        ui->p1name->show();
        ui->p2name->hide();
    } else if (val < 3) {
        ui->p2name->show();
        ui->p3name->hide();
    } else if (val < 4) {
        ui->p3name->show();
        ui->p4name->hide();
    } else {
        ui->p4name->show();
    }
}

bool SettingsWindow::check_player_status(int val)
{
    if (ui->player_n->value() > val) {
        return true;
    } else {
        return false;
    }
}

void SettingsWindow::set_player_names()
{
    players.push_back(std::make_pair<std::string, bool>(ui->p1name->toPlainText().toStdString(), check_player_status(0)));
    players.push_back(std::make_pair<std::string, bool>(ui->p2name->toPlainText().toStdString(), check_player_status(1)));
    players.push_back(std::make_pair<std::string, bool>(ui->p3name->toPlainText().toStdString(), check_player_status(2)));
    players.push_back(std::make_pair<std::string, bool>(ui->p4name->toPlainText().toStdString(), check_player_status(3)));
}

void SettingsWindow::on_player_n_valueChanged(int val)
{
    if (val + ui->bot_n->value() > 4) {
        ui->bot_n->setValue(4 - val);
    }

    this->update_UI(val);
}

void SettingsWindow::on_bot_n_valueChanged(int val)
{
    if (val + ui->player_n->value() > 4) {
        ui->player_n->setValue(4 - val);
    }

    this->update_UI(ui->player_n->value());
}

void SettingsWindow::on_p1name_textChanged()
{
    if (players.size() > 0) {
        players[0] = std::make_pair<std::string, bool>(ui->p1name->toPlainText().toStdString(), check_player_status(0));
    }
}

void SettingsWindow::on_p2name_textChanged()
{
    if (players.size() > 1) {
        players[1] = std::make_pair<std::string, bool>(ui->p2name->toPlainText().toStdString(), check_player_status(1));
    }
}

void SettingsWindow::on_p3name_textChanged()
{
    if (players.size() > 2) {
        players[2] = std::make_pair<std::string, bool>(ui->p3name->toPlainText().toStdString(), check_player_status(2));
    }
}

void SettingsWindow::on_p4name_textChanged()
{
    if (players.size() > 3) {
        players[3] = std::make_pair<std::string, bool>(ui->p4name->toPlainText().toStdString(), check_player_status(3));
    }
}

void SettingsWindow::on_playB_clicked()
{
    std::cout << "Play button clicked" << std::endl;

    data_struct export_data;

    for (int i = 0; i < ui->bot_n->value() + ui->player_n->value(); ++i) {
        export_data.players.push_back(players[i]);
    }

    export_data.map_seed = ui->seed_n->value();
    export_data.chosen_preset = ui->tile_preset->currentIndex();
    export_data.win_condition = ui->win_preset->currentIndex();

    emit send_data(export_data);
    hide();
    settings_created = true;
}
