#ifndef GAMEEVENTHANDLER_H
#define GAMEEVENTHANDLER_H

#include "core/gameobject.h"
#include "interfaces/igameeventhandler.h"
#include "core_st/resourcemaps.h"
#include "objectmanager.h"

namespace Student {

class GameEventHandler: public Course::iGameEventHandler
{
public:

    GameEventHandler();


    /**
     * @brief Modify Player's resource. Can be used to both sum or subtract.
     * @param player Pointer to the Player whose resource is being modified.
     * @param resource Defines the modified resource.
     * @param amount Defines the amount of change.
     * @post Exception guarantee: Basic
     * @return
     * True - Modification was succesful. \n
     * False - Modification failed. \n
     */
    bool modifyResource(std::shared_ptr<Course::PlayerBase> player,
                                Course::BasicResource resource,
                                int amount) override;

    /**
     * @brief Modify Player's resources. Can be used to both sum or subtract
     * @param player Pointer to the Player whose resources are being modified.
     * @param resources ResourceMap containing change amounts.
     * @return
     * True - Modification was succesful. \n
     * False - Modification failed. \n
     */
    bool modifyResources(std::shared_ptr<Course::PlayerBase> player,
                         Course::ResourceMap resources) override;

    void initializeResources(unsigned int p_count);

    std::vector<int> getResources(unsigned int p_i);

    bool addToResourceMap(unsigned int p_i, Course::ResourceMap resources);
    bool substractFromResourceMap(unsigned int p_i, Course::ResourceMap resources);

    bool addResources(unsigned int p_i, std::vector<std::shared_ptr<Course::GameObject>> objects, std::shared_ptr<Student::ObjectManager> object_manager);
private:

    std::weak_ptr<Course::PlayerBase> m_owner;

    std::vector<Course::ResourceMap> m_resources_;

};

} // namespace Student

#endif // GAMEEVENTHANDLER_H
