#include "graphics/mapitem.h"

#include <QFile>
#include <QImage>
#include <QImageReader>
#include <QDebug>

#include <tuple>

namespace Student {

std::map<std::string, QColor> MapItem::c_mapcolors = {
    {std::string("Forest"), QColor(20,60,20)},
    {std::string("Grassland"), QColor(20,200,20)},
    {std::string("Field"), QColor(200,200,40)},
    {std::string("Mountain"), QColor(150,150,150)},
    {std::string("Rocks"), QColor(90,90,90)},
    {std::string("Water"), QColor(30,30,240)},
};

MapItem::MapItem(const std::shared_ptr<Course::TileBase> &obj, int size ):
    m_gameobject(obj), m_scenelocation(m_gameobject->getCoordinatePtr()->asQpoint()), m_size(size)
{
    addNewColor(m_gameobject->getType());
}

QRectF MapItem::boundingRect() const
{
    return QRectF(m_scenelocation * m_size, m_scenelocation * m_size + QPoint(m_size, m_size));
}

void MapItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED( option ); Q_UNUSED( widget );

    painter->setBrush(QBrush(c_mapcolors.at(m_gameobject->getType())));
    painter->drawRect(boundingRect());


    auto type = m_gameobject->getType();
    auto bRect = boundingRect();
    if ( type == "Forest" ){
        painter->setBrush(QBrush(QColor(20,40,20)));
        auto r = bRect;
        float offsets[] = {-15,15, 15,-15};
        for(int i = 0; i < 2; ++i) {
            auto c = r.center();
            c.setX(c.x() + offsets[i*2+0]);
            c.setY(c.y() + offsets[i*2+1]);
            painter->drawEllipse(c, 3, 3);
        }
    }
    painter->setBrush(QBrush(QColor(0,0,0)));
    painter->drawText(bRect.adjusted(4,0,0,0), QString(type[0]));

    std::string postfix;
    QImageReader imgReader;
    auto loadImg = [&imgReader,&postfix](std::string imgn){
        imgReader.setFileName(QString::fromStdString(imgn+postfix));
        return imgReader.read();
    };

    if (m_gameobject->getBuildingCount() > 0) {
        auto building = m_gameobject->getBuildings()[0];

        unsigned int owner_id = static_cast<PlayerC*>(building->getOwner().get())->getPlayerIndex();
        postfix = std::to_string(owner_id+1)+".png";

        auto img_str = building->getType();
        if(img_str == "HeadQuarters") {
            img_str = "hq";
        }
        img_str[0] = std::tolower(img_str[0]);
        img_str = std::string(":/")+img_str;
        QImage img_build = loadImg(img_str);
        painter->drawImage(bRect.adjusted(15,3,-15,-27),img_build);
    }

    if (m_gameobject->getWorkerCount() > 0) {
        auto worker = m_gameobject->getWorkers()[0];
        unsigned int owner_id = static_cast<PlayerC*>(worker->getOwner().get())->getPlayerIndex();
        postfix = std::to_string(owner_id+1)+".png";
        auto img_str = worker->getType();
        img_str[0] = std::tolower(img_str[0]);
        img_str = std::string(":/")+img_str;
        QImage img_cavalry = loadImg(img_str);
        painter->drawImage(bRect.adjusted(15,27,-15,-3),img_cavalry);
    }
}

const std::shared_ptr<Course::TileBase> &MapItem::getBoundObject()
{
    return m_gameobject;
}

void MapItem::updateLoc()
{
    if ( !m_gameobject ){
        delete this;
    } else {
        //update(boundingRect()); // Test if necessary
        update();
        m_scenelocation = m_gameobject->getCoordinate().asQpoint();
    }
}

bool MapItem::isSameObj(std::shared_ptr<Course::TileBase> obj)
{
    return obj == m_gameobject;
}

int MapItem::getSize() const
{
    return m_size;
}

void MapItem::setSize(int size)
{
    if ( size > 0 && size <= 500 ){
        m_size = size;
    }
}

void MapItem::addNewColor(std::string type)
{
    if ( c_mapcolors.find(type) == c_mapcolors.end() ){
        std::size_t hash = std::hash<std::string>{}(type);
        c_mapcolors.insert({type, QColor((hash & 0xFF0000) >> 16, (hash & 0x00FF00 ) >> 8, (hash & 0x0000FF))});

    }
}

}
