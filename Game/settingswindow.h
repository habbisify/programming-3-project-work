#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QTabWidget>
#include <vector>
#include <string>

#include "data_struct.h"

#include "ui_settingswindow.h"

#include "iostream"

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QTabWidget
{
    Q_OBJECT

public:
    explicit SettingsWindow(QWidget *parent = 0);
    ~SettingsWindow();

    void update_UI(int val);

    void set_player_names();

    bool check_player_status(int val);

    bool settings_created = false;

private slots:

    void on_player_n_valueChanged(int val);

    void on_bot_n_valueChanged(int val);

    void on_playB_clicked();

    void on_p1name_textChanged();

    void on_p2name_textChanged();

    void on_p3name_textChanged();

    void on_p4name_textChanged();

signals:

    void send_data(data_struct ds);

private:
    Ui::SettingsWindow *ui;

    std::vector<std::pair<std::string, bool>> players;
};

#endif // SETTINGSWINDOW_H
