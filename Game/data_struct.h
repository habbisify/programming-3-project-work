#ifndef DATA_STRUCT_H
#define DATA_STRUCT_H

#include <vector>
#include <string>

struct data_struct
{
    data_struct(): players(), map_seed(0), chosen_preset(0), win_condition(0) {}
    std::vector<std::pair<std::string, bool>> players;
    int map_seed = 0; // [0, 999]
    int chosen_preset = 1; // [0, 4]
    int win_condition = 2; // [0, 1]
};

#endif // DATA_STRUCT_H
