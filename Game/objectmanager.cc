#include "objectmanager.h"

namespace Student {

ObjectManager::ObjectManager()
{

}

void ObjectManager::addTiles(const std::vector<std::shared_ptr<Course::TileBase> > &tiles)
{
    for (auto it = tiles.begin(); it != tiles.end(); ++it)
    {
        map_.insert({(*it)->getCoordinate(), *it});

        map_by_id_.insert({(*it)->ID, (*it)});
    }
}

std::shared_ptr<Course::TileBase> ObjectManager::getTile(const Course::Coordinate &coordinate)
{
    auto it = map_.find(coordinate);

    if (it == map_.end())
    {
        return nullptr;
    }
    else
    {
        return it->second;
    }
}

std::shared_ptr<Course::TileBase> ObjectManager::getTile(const Course::ObjectId &id)
{
    auto it = map_by_id_.find(id);

    if (it == map_by_id_.end())
    {
        return nullptr;
    }
    else
    {
        return it->second;
    }
}

std::vector<std::shared_ptr<Course::TileBase> > ObjectManager::getTiles(const std::vector<Course::Coordinate> &coordinates)
{
    std::vector<std::shared_ptr<Course::TileBase>> v;

    for (auto coordinate_it = coordinates.begin();
         coordinate_it != coordinates.end(); ++coordinate_it)
    {
        auto it = map_.find(*coordinate_it);

        if (it != map_.end())
        {
            v.push_back(it->second);
        }
    }
    return v;
}

void ObjectManager::addPlayers() {
    unsigned int id = 0;
    for (auto p : game_conf_.players) {
        Player pl = std::make_shared<Student::PlayerC>(p.first);
        pl->initID(id);
        players_.push_back(pl);
        id++;
    }
}

Player ObjectManager::getPlayer(unsigned int player_index)
{
    return players_.at(player_index);
}

unsigned int ObjectManager::getPlayerCount()
{
    return players_.size();
}

int ObjectManager::getSeed()
{
    return game_conf_.map_seed;
}

std::vector<int> ObjectManager::getMapWeights()
{
    std::vector<int> map_weights;
    int present_index = game_conf_.chosen_preset;

    // FOREST, GRASSLAND, FIELD, MOUNTAIN, ROCKS, WATER
    // preset 20,10,20,10,10,5 GREEN HILLS
    // preset 10,30,20,8,8,5 ARABIA
    // preset 50,8,6,4,4,0 FOREST NOTHING
    // preset 15,10,5,40,50,20 GOLD RUSH
    // preset 30,8,60,1,1,10 KYLMAKOSKI
    // preset RANDOM

    switch (present_index) {
    case 0:
        // GREEN HILLS
        map_weights = {20,10,20,10,10,5};
        break;
    case 1:
        // ARABIA
        map_weights = {10,60,20,5,4,1};
        break;
    case 2:
        // FOREST NOTHING
        map_weights = {50,8,6,4,4,0};
        break;
    case 3:
        // MOUNT EVEREST
        map_weights = {15,10,5,40,50,1};
        break;
    case 4:
        // KYLMAKOSKI
        map_weights = {30,8,60,1,1,10};
        break;
    case 5:
        // RANDOM
        for (unsigned int i = 0; i < 6; ++i)
        {
            int r = rand() % 101;
            map_weights.push_back(r);
        }
        break;
    default:
        map_weights = {20,10,20,10,10,5};
        break;
    }

    return map_weights;
}

bool ObjectManager::addBuilding(std::shared_ptr<Course::BuildingBase> building,
                                std::shared_ptr<Course::TileBase> tile,
                                unsigned int player_index = 0) {
    unsigned int bld_count = tile->getBuildingCount();
    try {
        tile->addBuilding(building);
    } catch (Course::IllegalAction&) {
        qDebug("Illegal building spot!");
    }

    if (tile->getBuildingCount() > bld_count) {
        buildings_.push_back(building);
        tile->setOwner(ObjectManager::getPlayer(player_index));

        return true;
    } else {
        return false;
    }
}

bool ObjectManager::removeBuilding(Course::ObjectId id) {
    int i = 0;
    int o_i = -1;
    for (auto building : buildings_) {
        if (building->ID == id) {
            o_i = i;
        }
        i++;
    }
    if (o_i != -1) {
        buildings_.erase(buildings_.begin() + o_i);
        return true;
    }
    return false;
}

bool ObjectManager::addUnit(std::shared_ptr<Course::WorkerBase> worker,
                                std::shared_ptr<Course::TileBase> tile,
                                unsigned int player_index) {
    unsigned int unit_count = tile->getWorkerCount();
    try {
        tile->addWorker(worker);
    } catch (Course::IllegalAction&) {
        qDebug("Illegal unit spot!");
    }

    if (tile->getWorkerCount() > unit_count) {
        workers_.push_back(worker);
        tile->setOwner(ObjectManager::getPlayer(player_index));

        return true;
    } else {
        return false;
    }
}

bool ObjectManager::setGameConf(data_struct ds)
{
    try {
        game_conf_.players = ds.players;
        game_conf_.map_seed = ds.map_seed;
        game_conf_.chosen_preset = ds.chosen_preset;
        game_conf_.win_condition = ds.win_condition;
    } catch (std::runtime_error&) {
        return false;
    }
    MAX_PLAYERS_ = ds.players.size();

    return true;
}

} // namespace Student
