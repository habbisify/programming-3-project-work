#pragma once

#include "tiles/tilebase.h"
#include "core_st/resourcemaps.h"


namespace Student {
using Course::Coordinate;
using Course::iGameEventHandler;
using Course::iObjectManager;
using Course::ResourceMap;
using Course::BuildingBase;

/**
 * @brief The Water class represents Water in the gameworld.
 *
 * Water has BasicProduction: \n
 * * Money = 0
 * * Food = 0
 * * Wood = 0
 * * Stone = 0
 * * Ore = 0
 *
 * Tile doesn't support buildings or units.
 */
class Water : public Course::TileBase
{
public:
    /**
     * @brief Disabled parameterless constructor.
     */
    Water() = delete;

    /**
     * @brief Constructor for the class.
     *
     * @param location is the Coordinate where the Tile is located in the game.
     * @param eventhandler points to the student's GameEventHandler.
     */
    Water(const Coordinate& location,
             const std::shared_ptr<iGameEventHandler>& eventhandler,
             const std::shared_ptr<iObjectManager>& objectmanager,
             const unsigned int& max_build = 0,
             const unsigned int& max_work = 0,
             const ResourceMap& production = Student::ConstResourceMaps::WATER_BP);

    /**
     * @brief Default destructor.
     */
    virtual ~Water() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;

    /**
     * @brief Adds a new building-object to the tile. Building in water adds
     * one hold-marker to the building.
     *
     * Phases: \n
     * 1. Check that there is space for the building. \n
     * 2. Call parent's addBuilding \n
     * 3. Add a HoldMarker for the building. \n
     *
     * @post Exception guarantee: Strong
     * @exception OwnerConflict - If the tile's ownership prevents placing the
     * \b building.
     * @exception NoSpace - If the tile doesn't have enough space for
     * the \b building.
     */
    void addBuilding(const std::shared_ptr<Course::BuildingBase>& building) override;

}; // class Water

} // namespace Student
