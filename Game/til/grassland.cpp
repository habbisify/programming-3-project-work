#include "til/grassland.h"

namespace Student {
using Course::Coordinate;
using Course::iGameEventHandler;
using Course::iObjectManager;
using Course::ResourceMap;

Grassland::Grassland(const Coordinate &location, const std::shared_ptr<iGameEventHandler> &eventhandler, const std::shared_ptr<iObjectManager> &objectmanager, const unsigned int &max_build, const unsigned int &max_work, const ResourceMap &production)
    : Course::Grassland(location, eventhandler, objectmanager, max_build, max_work, production)
{

}

} // namespace Student
