#include "mountain.h"


namespace Student {
using Course::Coordinate;
using Course::iGameEventHandler;
using Course::iObjectManager;
using Course::ResourceMap;
using Course::BuildingBase;

Mountain::Mountain(const Coordinate& location,
               const std::shared_ptr<iGameEventHandler>& eventhandler,
               const std::shared_ptr<iObjectManager>& objectmanager,
               const unsigned int& max_build,
               const unsigned int& max_work,
               const ResourceMap& production):
    TileBase(location,
             eventhandler,
             objectmanager,
             max_build,
             max_work,
             production)
{
}

std::string Mountain::getType() const
{
    return "Mountain";
}

void Mountain::addBuilding(const std::shared_ptr<BuildingBase>& building)
{
    TileBase::addBuilding(building);
    building->addHoldMarkers(1);
}

} // namespace Student
