#pragma once

#include "tiles/forest.h"
#include "core_st/resourcemaps.h"

namespace Student {

class Forest : public Course::Forest
{
public:
    Forest(const Course::Coordinate& location,
           const std::shared_ptr<Course::iGameEventHandler>& eventhandler,
           const std::shared_ptr<Course::iObjectManager>& objectmanager,
           const unsigned int& max_build = 1,
           const unsigned int& max_work = 1,
           const Course::ResourceMap& production = Student::ConstResourceMaps::FOREST_BP);
};

} // namespace Student
