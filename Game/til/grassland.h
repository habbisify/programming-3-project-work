#pragma once

#include "tiles/grassland.h"
#include "core_st/resourcemaps.h"

namespace Student {

class Grassland : public Course::Grassland
{
public:
    Grassland(const Course::Coordinate& location,
              const std::shared_ptr<Course::iGameEventHandler>& eventhandler,
              const std::shared_ptr<Course::iObjectManager>& objectmanager,
              const unsigned int& max_build = 1,
              const unsigned int& max_work = 1,
              const Course::ResourceMap& production = Student::ConstResourceMaps::GRASSLAND_BP);
};

} // namespace Student
