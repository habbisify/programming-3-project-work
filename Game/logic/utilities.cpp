#include "utilities.h"

namespace Student {

bool checkIfNeighbors(Course::Coordinate c1, Course::Coordinate c2) {
    bool in_neighbors = false;
    for (auto c1n : c1.neighbours()) {
        if (c1n.x() >= 0 && c1n.y() >= 0) {
            if (c2.x() == c1n.x() and c2.y() == c1n.y()) {
                in_neighbors = true;
            }
        }
    }
    return in_neighbors;
}

Course::Coordinate uintToCoord(unsigned int map_item_selected) {
    int selected_x = -2;
    int selected_y = -2;
    if (std::to_string(map_item_selected).size() == 2) {
        selected_x = std::stoi(std::to_string(map_item_selected).substr(0,1));
        selected_y = std::stoi(std::to_string(map_item_selected).substr(1,1));
    } else {
        selected_x = 0;
        selected_y = map_item_selected;
    }
    Course::Coordinate c(selected_x,selected_y);
    return c;
}

std::vector<Course::Coordinate> returnObjCoords(std::string obj_type, unsigned int p_i, std::shared_ptr<Student::ObjectManager> obj_manager) {
    std::vector<Course::Coordinate> obj_crds = {};
    if (obj_type == "ALL_BUILDINGS") {
        for (auto obj : obj_manager->getPlayer(p_i)->getObjects()) {
            std::vector<std::string> bld_names = {"HeadQuarters","Outpost","Farm","Sawmill","Mine","Quarry"};
            if (std::find(bld_names.begin(), bld_names.end(), obj->getType()) != bld_names.end()) {
                obj_crds.push_back(obj->getCoordinate());
            }
        }
    } else {
        for (auto obj : obj_manager->getPlayer(p_i)->getObjects()) {
            if (obj->getType() == obj_type) {
                obj_crds.push_back(obj->getCoordinate());
            }
        }
    }
    return obj_crds;
}

bool checkHQDefeat(unsigned int p_i, std::shared_ptr<Student::ObjectManager> obj_manager) {
    std::vector<std::shared_ptr<Course::GameObject>> buildings = obj_manager->getPlayer(p_i)->getObjects();
    for (auto b : buildings) {
        if (b->getType() == "HeadQuarters" && b->getCoordinatePtr() != nullptr) {
            return false;
        }
    }
    return true;
}

} // namespace Student
