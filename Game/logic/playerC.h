#pragma once

#include <string>
#include <vector>
#include <memory>

#include "core/playerbase.h"
#include "core/gameobject.h"
#include "core_st/resourcemaps.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;

class PlayerC : public Course::PlayerBase
{
public:
    /**
     * @brief Constructor for the class
     * @param name A std::string for player's name
     * @param objects (optional) A std::vector of shared-pointers to
     * GameObjects.
     */
    explicit PlayerC(const std::string& name,
            const std::vector<std::shared_ptr<Course::GameObject> > objects = {});

    /**
     * @brief Default destructor
     */
    virtual ~PlayerC() = default;

    void initID(unsigned int id);
    void updateResources(ResourceMap res);
    void updateResources(ResourceMapDouble res);
    ResourceMap getResources();
    unsigned int getPlayerIndex();

private:
    std::string m_name_ = "";
    std::vector<std::weak_ptr<Course::GameObject> > m_objects_ = {};

    unsigned int p_id_ = 0;

    ResourceMap resources_ = Student::ConstResourceMaps::INIT_RES;

};
} // namespace Student
