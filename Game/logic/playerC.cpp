#include "playerC.h"

namespace Student {

PlayerC::PlayerC(const std::string& name,
                 const std::vector<std::shared_ptr<Course::GameObject> > objects) :
      PlayerBase(
                 name,
                 objects)
{
}

void PlayerC::initID(unsigned int id) {
    p_id_ = id;
}

void PlayerC::updateResources(ResourceMap res) {
    ResourceMap resources_ = Course::mergeResourceMaps(resources_,res);
}
void PlayerC::updateResources(ResourceMapDouble res) {
    ResourceMap resources_ = Course::multiplyResourceMap(resources_,res);
}

Course::ResourceMap PlayerC::getResources()
{
    return resources_;
}

unsigned int PlayerC::getPlayerIndex() {
    return p_id_;
}

} // namespace Student
