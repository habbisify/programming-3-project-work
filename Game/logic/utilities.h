#pragma once

#include <algorithm>
#include <string>

#include <core/coordinate.h>
#include <objectmanager.h>

namespace Student {

// Checks if coordinates c1 and c2 are adjacent safely (takes values below 0 into account)
bool checkIfNeighbors(Course::Coordinate c1, Course::Coordinate c2);

// Converts an unsigned int into Coordinate
Course::Coordinate uintToCoord(unsigned int map_item_selected);

// Helper method that returns all objects of obj_type controlled by player with ID of p_i.
// If obj_type = "ALL_BUILDINGS" then returns vector of all buildings of player
std::vector<Course::Coordinate> returnObjCoords(std::string obj_type, unsigned int p_i, std::shared_ptr<Student::ObjectManager> obj_manager);

// Returns true if player p_i has lost all their HQs
bool checkHQDefeat(unsigned int p_i, std::shared_ptr<Student::ObjectManager> obj_manager);
} // namespace Student
