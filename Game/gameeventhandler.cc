#include "gameeventhandler.h"

#include <QDebug>

namespace Student {

GameEventHandler::GameEventHandler()
{

}

/*
GameEventHandler::GameEventHandler(const GameEventHandler &original, const std::shared_ptr<PlayerBase>& owner):
    EVENTHANDLER(original.EVENTHANDLER)
{
    m_owner = owner;
}
*/

bool GameEventHandler::modifyResource(std::shared_ptr<Course::PlayerBase> player,
                                      Course::BasicResource resource,
                                      int amount)
{
    (void)player; //this solution is to prevent warnings from unused variables in code that was not finished
    (void)resource;
    (void)amount;
    return false;
}

bool GameEventHandler::modifyResources(std::shared_ptr<Course::PlayerBase> player,
                                       Course::ResourceMap resources)
{
    (void)player; //this solution is to prevent warnings from unused variables in code that was not finished
    (void)resources;
    return false;
}

bool GameEventHandler::addResources(unsigned int p_i, std::vector<std::shared_ptr<Course::GameObject>> objects, std::shared_ptr<Student::ObjectManager> object_manager) {
    std::vector<Course::Coordinate> worktiles = {};
    for (auto object : objects) {
        qDebug() << QString::fromStdString(object->getType());
        if (object->getType() == "Worker") {
            worktiles.push_back(object->getCoordinate());
        }
        if (object->getType() == "HeadQuarters") {
            auto tile = object_manager->getTile(object->getCoordinate());
            if (tile->getWorkerCount() == 0) {
                addToResourceMap(p_i, Student::ConstResourceMaps::HQ_PRODUCTION);
            } else {
                addToResourceMap(p_i, Student::ConstResourceMaps::HQ_PRODUCTION_W_WORKER);
            }
        }
    }
    for (auto coord : worktiles) {
        auto tile = object_manager->getTile(coord);

        qDebug() << QString::fromStdString(tile->getType());
        if (tile->getType() == "Grassland") {
            if (tile->getBuildings().size() >= 1) {
                if (tile->getBuildings()[0]->getType() == "Farm") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::FARM_PRODUCTION_GRASS);
                } else if (tile->getBuildings()[0]->getType() == "Sawmill") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::SAWMILL_PRODUCTION_GRASS);
                }
            }
            addToResourceMap(p_i, Student::ConstResourceMaps::GRASSLAND_BP);

        } else if (tile->getType() == "Forest") {
            if (tile->getBuildings().size() >= 1) {
                if (tile->getBuildings()[0]->getType() == "Sawmill") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::SAWMILL_PRODUCTION_FOREST);
                }
            }
            addToResourceMap(p_i, Student::ConstResourceMaps::FOREST_BP);

        } else if (tile->getType() == "Field") {
            if (tile->getBuildings().size() >= 1) {
                if (tile->getBuildings()[0]->getType() == "Farm") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::FARM_PRODUCTION_FIELD);
                }
            }
            addToResourceMap(p_i, Student::ConstResourceMaps::FIELD_BP);

        } else if (tile->getType() == "Mountain") {
            if (tile->getBuildings().size() >= 1) {
                if (tile->getBuildings()[0]->getType() == "Mine") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::MINE_PRODUCTION_MOUNTAIN);
                } else if (tile->getBuildings()[0]->getType() == "Quarry") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::QUARRY_PRODUCTION_MOUNTAIN);
                }
            }
            addToResourceMap(p_i, Student::ConstResourceMaps::MOUNTAIN_BP);

        } else if (tile->getType() == "Rocks") {
            if (tile->getBuildings().size() >= 1) {
                if (tile->getBuildings()[0]->getType() == "Quarry") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::QUARRY_PRODUCTION_ROCKS);
                } else if (tile->getBuildings()[0]->getType() == "Mine") {
                    addToResourceMap(p_i, Student::ConstResourceMaps::MINE_PRODUCTION_ROCKS);
                }
            }
            addToResourceMap(p_i, Student::ConstResourceMaps::ROCKS_BP);

        } else if (tile->getType() == "Water") {
            addToResourceMap(p_i, Student::ConstResourceMaps::WATER_BP);
        }
    }
    return true;
}

bool GameEventHandler::addToResourceMap(unsigned int p_i, Course::ResourceMap resources)
{
    if (m_resources_.size() <= p_i) {
        return false;
    } else {
        auto user_res = m_resources_.at(p_i);
        Course::ResourceMap r = user_res;

        for(auto iter = user_res.begin(); iter != user_res.end(); iter++)
        {
            for (auto iter2 = resources.begin(); iter2 != resources.end(); iter2++)
            {
                if (iter->first == iter2->first) {
                    iter->second += iter2->second;
                    r.at(iter->first) = iter->second;
                }
            }
        }
        m_resources_.at(p_i) = r;

        return true;
    }
}

bool GameEventHandler::substractFromResourceMap(unsigned int p_i, Course::ResourceMap resources)
{
    if (m_resources_.size() <= p_i) {
        return false;
    } else {
        auto user_res = m_resources_.at(p_i);
        Course::ResourceMap r = user_res;

        for(auto iter = user_res.begin(); iter != user_res.end(); iter++)
        {
            for (auto iter2 = resources.begin(); iter2 != resources.end(); iter2++)
            {
                if (iter->first == iter2->first) {
                    if (iter2->second > iter->second) {
                        return false;
                    }
                    iter->second -= iter2->second;
                    r.at(iter->first) = iter->second;
                }
            }
        }
        m_resources_.at(p_i) = r;

        return true;
    }
}

void GameEventHandler::initializeResources(unsigned int p_count)
{
    for (unsigned int p = 0; p < p_count; p++)
    {
        m_resources_.push_back(Student::ConstResourceMaps::INIT_RES);
    }
}

std::vector<int> GameEventHandler::getResources(unsigned int p_i)
{
    if (m_resources_.size() <= p_i) {
        return {};
    }
    std::vector<int> resources = {};
    auto user_res = m_resources_.at(p_i);
    for(auto iter : user_res) {
        resources.push_back(iter.second);
    }
    return resources;
}


}
