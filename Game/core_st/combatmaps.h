#ifndef COMBATMAPS_H
#define COMBATMAPS_H

#include <map>

namespace Student {

enum Combattables {
    NONE = 0,
    HQ = 1,
    OUTPOST = 2,
    FARM = 3,
    MINE = 4,
    QUARRY = 5,
    SAWMILL = 6,
    WORKER = 10,
    SPEARMAN = 11,
    CAVALRY = 12,
    ARCHER = 13
};

/**
 * @brief CombatMap is an alias for std::map<Combattables, int>
 */
using CombatMap = std::map<Combattables, float>;

namespace CombatMaps {
const CombatMap EMPTY = {};

const CombatMap WORKER_CM = {
    {HQ, 0.01},
    {OUTPOST, 0.05},
    {FARM, 0.10},
    {MINE, 0.10},
    {QUARRY, 0.10},
    {SAWMILL, 0.10},
    {WORKER, 0.50},
    {SPEARMAN, 0.03},
    {CAVALRY, 0.05},
    {ARCHER, 0.02}
};

const CombatMap SPEARMAN_CM = {
    {HQ, 0.50},
    {OUTPOST, 0.80},
    {FARM, 0.40},
    {MINE, 0.40},
    {QUARRY, 0.40},
    {SAWMILL, 0.40},
    {WORKER, 0.97},
    {SPEARMAN, 0.50},
    {CAVALRY, 0.75},
    {ARCHER, 0.25}
};

const CombatMap CAVALRY_CM = {
    {HQ, 0.30},
    {OUTPOST, 0.60},
    {FARM, 0.60},
    {MINE, 0.60},
    {QUARRY, 0.60},
    {SAWMILL, 0.60},
    {WORKER, 0.95},
    {SPEARMAN, 0.25},
    {CAVALRY, 0.50},
    {ARCHER, 0.75}
};

const CombatMap ARCHER_CM = {
    {HQ, 0.10},
    {OUTPOST, 0.30},
    {FARM, 0.90},
    {MINE, 0.90},
    {QUARRY, 0.90},
    {SAWMILL, 0.90},
    {WORKER, 0.98},
    {SPEARMAN, 0.75},
    {CAVALRY, 0.25},
    {ARCHER, 0.50}
};
} // namespace CombatMaps
} // namespace Student

#endif // COMBATMAPS_H
