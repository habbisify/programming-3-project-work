#ifndef ST_RESOURCEMAPS_H
#define ST_RESOURCEMAPS_H

#include "core/basicresources.h"

namespace Student {

namespace ConstResourceMaps {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource::MONEY;
using Course::BasicResource::FOOD;
using Course::BasicResource::WOOD;
using Course::BasicResource::STONE;
using Course::BasicResource::ORE;

const Course::ResourceMap EMPTY = {};

// Initial resources
const ResourceMap INIT_RES = {
    {MONEY, 500},
    {FOOD, 500},
    {WOOD, 500},
    {STONE, 500},
    {ORE, 500}
};

// Building - HeadQuarters
const ResourceMap HQ_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 1500},
    {STONE, 1500},
    {ORE, 1500}
};
const ResourceMap HQ_PRODUCTION = {
    {MONEY, 50},
    {FOOD, 10}
};

const ResourceMap HQ_PRODUCTION_W_WORKER = {
    {MONEY, 150},
    {FOOD, 50}
};

// Building - Outpost
const ResourceMap OUTPOST_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 600},
    {STONE, 600},
    {ORE, 600}
};

const ResourceMap OUTPOST_PRODUCTION = {
    {MONEY, 0}
};

// Building - Farm
const ResourceMap FARM_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 400},
    {STONE, 100},
    {ORE, 100}
};

const ResourceMap FARM_PRODUCTION_FIELD = {
    {FOOD, 100}
};

const ResourceMap FARM_PRODUCTION_GRASS = {
    {FOOD, 50}
};

// Building - Mine
const ResourceMap MINE_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 100},
    {STONE, 500},
    {ORE, 0}
};

const ResourceMap MINE_PRODUCTION_MOUNTAIN = {
    {ORE, 100}
};

const ResourceMap MINE_PRODUCTION_ROCKS = {
    {ORE, 25}
};

// Building - Quarry
const ResourceMap QUARRY_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 100},
    {STONE, 0},
    {ORE, 500}
};

const ResourceMap QUARRY_PRODUCTION_MOUNTAIN = {
    {STONE, 25}
};

const ResourceMap QUARRY_PRODUCTION_ROCKS = {
    {STONE, 100}
};

// Building - Sawmill
const ResourceMap SAWMILL_BUILD_COST = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 0},
    {STONE, 300},
    {ORE, 300}
};

const ResourceMap SAWMILL_PRODUCTION_FOREST = {
    {WOOD, 100}
};

const ResourceMap SAWMILL_PRODUCTION_GRASS = {
    {WOOD, 25}
};

// Worker - BasicWorker
const ResourceMapDouble WORKER_EFFICIENCY = {
    {MONEY, 1.00},
    {FOOD, 1.00},
    {WOOD, 1.00},
    {STONE, 1.00},
    {ORE, 1.00},
};

const ResourceMap WORKER_RECRUITMENT_COST = {
    {MONEY, 0},
    {FOOD, 300},
    {WOOD, 0},
    {STONE, 0},
    {ORE, 0}
};

const ResourceMap SPEARMAN_RECRUITMENT_COST = {
    {MONEY, 200},
    {FOOD, 300},
    {WOOD, 150},
    {STONE, 100},
    {ORE, 0}
};

const ResourceMap CAVALRY_RECRUITMENT_COST = {
    {MONEY, 300},
    {FOOD, 500},
    {WOOD, 100},
    {STONE, 0},
    {ORE, 20}
};

const ResourceMap ARCHER_RECRUITMENT_COST = {
    {MONEY, 200},
    {FOOD, 300},
    {WOOD, 250},
    {STONE, 100},
    {ORE, 100}
};

const ResourceMap GRASSLAND_BP = {
    {MONEY, 0},
    {FOOD, 50},
    {WOOD, 25},
    {STONE, 0},
    {ORE, 0}
};

const ResourceMap FOREST_BP = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 100},
    {STONE, 0},
    {ORE, 0}
};

const ResourceMap FIELD_BP = {
    {MONEY, 0},
    {FOOD, 100},
    {WOOD, 0},
    {STONE, 0},
    {ORE, 0}
};

const ResourceMap MOUNTAIN_BP = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 0},
    {STONE, 25},
    {ORE, 100}
};

const ResourceMap ROCKS_BP = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 0},
    {STONE, 100},
    {ORE, 25}
};

const ResourceMap WATER_BP = {
    {MONEY, 0},
    {FOOD, 0},
    {WOOD, 0},
    {STONE, 0},
    {ORE, 0}
};

}
} // namespace Student
#endif // RESOURCEMAPS_H
