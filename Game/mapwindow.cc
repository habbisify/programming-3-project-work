#include "mapwindow.hh"

using namespace Student::ConstResourceMaps;

MapWindow::MapWindow(QWidget *parent):
    QMainWindow(parent),
    m_ui(new Ui::MapWindow),
    m_GEHandler(std::make_shared<Student::GameEventHandler>()),
    m_ObjManager(std::make_shared<Student::ObjectManager>()),
    m_simplescene(new Student::GameScene(this)),
    m_rnd_engine(rand()),
    m_rnd_dist(0.0, 1.0)
{
    m_ObjManager->addPlayers();

    gameScene_ = m_simplescene.get();

    //QObject::connect(m_simplescene, m_simplescene->send_data, this, this->getMapElement);
}

MapWindow::~MapWindow()
{
    delete m_ui;
}

void MapWindow::import_data(data_struct ds)
{
    qDebug() << QString("Imported data");

    m_ObjManager->setGameConf(ds);
    m_ui->setupUi(this);

    unsigned int x = 10;
    unsigned int y = 10;
    drawMap(x, y);

    m_ui->graphicsView->setScene(gameScene_);

}

void MapWindow::drawMap(unsigned int x, unsigned int y)
{
    Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();

    constructMap(&gen);

    const int seed = m_ObjManager->getSeed();

    gen.generateMap(x,y,seed,m_ObjManager,m_GEHandler);

    m_ObjManager->addPlayers();

    for (unsigned int i=0; i<x; ++i)
    {
        for (unsigned int j=0; j<y; ++j)
        {
            std::shared_ptr<Course::TileBase> obj = m_ObjManager->getTile(Course::Coordinate(i, j));
            auto tileItem = new Student::MapItem(obj);
            gameScene_->addItem(tileItem);
        }
    }

    initializeGame();
}

void MapWindow::constructMap(Course::WorldGenerator *gen)
{
    std::vector<int> map_weights = m_ObjManager->getMapWeights();

    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    if (map_weights.size() == 6)
    {
        gen->addConstructor<Student::Forest>(map_weights[0]);
        gen->addConstructor<Student::Grassland>(map_weights[1]);
        gen->addConstructor<Student::Field>(map_weights[2]);
        gen->addConstructor<Student::Mountain>(map_weights[3]);
        gen->addConstructor<Student::Rocks>(map_weights[4]);
        gen->addConstructor<Student::Water>(map_weights[5]);
    } else {
        // Execution should never fall here; this is for safety
        gen->addConstructor<Student::Forest>(20);
        gen->addConstructor<Student::Grassland>(10);
        gen->addConstructor<Student::Field>(10);
        gen->addConstructor<Student::Mountain>(10);
        gen->addConstructor<Student::Rocks>(10);
        gen->addConstructor<Student::Water>(5);
    }
}

void MapWindow::setGEHandler(std::shared_ptr<Student::GameEventHandler> nHandler)
{
    m_GEHandler = nHandler;
}

void MapWindow::setSize(int width, int height)
{
    m_simplescene->setSize(width, height);
}

void MapWindow::setScale(int scale)
{
    m_simplescene->setScale(scale);
}

void MapWindow::resize()
{
    m_simplescene->resize();
}

void MapWindow::updateItem(std::shared_ptr<Course::TileBase> obj)
{
    m_simplescene->updateItem(obj);
    m_ui->graphicsView->hide();
    m_ui->graphicsView->show();
}

void MapWindow::removeItem(std::shared_ptr<Course::TileBase> obj)
{
    m_simplescene->removeItem(obj);
}

void MapWindow::drawItem( std::shared_ptr<Course::TileBase> obj)
{
    m_simplescene->drawItem(obj);
}

void MapWindow::initializeGame()
{
    // Initialize resources
    m_GEHandler->initializeResources(m_ObjManager->getPlayerCount());

    unsigned int p = 0;
    for (auto p1=p; p1 < m_ObjManager->getPlayerCount(); ++p1)
    {
        auto n = m_ObjManager->getPlayer(p1)->getName();
        QString name = QString::fromStdString(m_ObjManager->getPlayer(p1)->getName());
        QTableWidgetItem *widg = new QTableWidgetItem(QString(name));
        m_ui->statsTable->setVerticalHeaderItem(p1, widg);

        initializePlayerHQ(p1);
        // Give player 1 worker placed on map
        initializePlayerWorker(p1);
        // Update resources to player
        updateResourceMaps(p1);
        p++;
    }
    for (auto p2 = p; p2 < 4; ++p2) {
        m_ui->statsTable->removeRow(p2);
    }
    updateTurnLabel();

    m_ui->textBrowser->setText("Crush the seven deadly sins! Good luck.");
}

void MapWindow::updateIcons()
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    // Updates icons to all unit buttons and building buttons
    m_ui->workerB->setIcon(QIcon(QString(":/worker")+QString::number(player_index_+1)+QString(".png")));
    m_ui->spearmanB->setIcon(QIcon(QString(":/spearman")+QString::number(player_index_+1)+QString(".png")));
    m_ui->archerB->setIcon(QIcon(QString(":/archer")+QString::number(player_index_+1)+QString(".png")));
    m_ui->cavalryB->setIcon(QIcon(QString(":/cavalry")+QString::number(player_index_+1)+QString(".png")));
    m_ui->HQB->setIcon(QIcon(QString(":/hq")+QString::number(player_index_+1)+QString(".png")));
    m_ui->outpostB->setIcon(QIcon(QString(":/outpost")+QString::number(player_index_+1)+QString(".png")));
    m_ui->farmB->setIcon(QIcon(QString(":/farm")+QString::number(player_index_+1)+QString(".png")));
    m_ui->sawmillB->setIcon(QIcon(QString(":/sawmill")+QString::number(player_index_+1)+QString(".png")));
    m_ui->quarryB->setIcon(QIcon(QString(":/quarry")+QString::number(player_index_+1)+QString(".png")));
    m_ui->mineB->setIcon(QIcon(QString(":/mine")+QString::number(player_index_+1)+QString(".png")));
}

void MapWindow::updateTurnLabel()
{
    QString s("Turn ");
    s.append(QString::number(turn_));
    s.append(" of player: ");
    s.append(QString::fromStdString(m_ObjManager->getPlayer(player_index_)->getName()));
    m_ui->turnLabel->setText(s);
}

void MapWindow::initializePlayerWorker(unsigned int p_i)
{
    auto tile_coord = getCoordinateForHQBuilding(p_i);
    auto tile = m_ObjManager->getTile(tile_coord);
    auto unit = std::make_shared<Student::Worker>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(p_i));
    m_ObjManager->addUnit(unit, tile, p_i);
    tile->setOwner(m_ObjManager->getPlayer(p_i));
    updateItem(tile);
    m_ObjManager->getPlayer(p_i)->addObject(unit);
}

void MapWindow::initializePlayerHQ(unsigned int p_i)
{
    auto tile_coord = getCoordinateForHQBuilding(p_i);
    auto tile = m_ObjManager->getTile(tile_coord);
    auto building = std::make_shared<Student::HeadQ>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(p_i));
    m_ObjManager->addBuilding(building, tile, p_i);
    tile->setOwner(m_ObjManager->getPlayer(p_i));
    updateItem(tile);
    m_ObjManager->getPlayer(p_i)->addObject(building);
}

void MapWindow::updateResourceMaps(unsigned int p_i)
{
    auto resources = m_GEHandler->getResources(p_i);
    for (unsigned int i = 0; i < resources.size(); i++)
    {
        QString amount = QString::fromStdString(std::to_string(resources.at(i)));
        QTableWidgetItem *widg = new QTableWidgetItem(QString(amount));
        m_ui->statsTable->setItem(p_i, i, widg);
    }

}

std::string MapWindow::getActiveElem() {
    return active_element_;
}

Course::Coordinate MapWindow::getCoordinateForHQBuilding(unsigned int p_i)
{
    Course::Coordinate coord(0,0);
    std::vector<std::pair<int,int>> default_coords = {std::make_pair(1,1), std::make_pair(8,8), std::make_pair(1,8), std::make_pair(8,1)};

    if (p_i >= m_ObjManager->getPlayerCount() || p_i >= default_coords.size()) {
        coord.set_x(0);
        coord.set_y(0);
    } else {
        Course::Coordinate tmp_coord(default_coords[p_i].first,default_coords[p_i].second);
        coord = MapWindow::getCoordinateForBuilding(tmp_coord, p_i);
    }

    return coord;
}

Course::Coordinate MapWindow::getCoordinateForBuilding(Course::Coordinate c, unsigned int p_i)
{
    auto tile = m_ObjManager->getTile(c);
    Course::Coordinate coord = tile->getCoordinate();
    // Checks if the tile contains water. Checks neighbour tiles.
    while (tile->getType() == "Water") {
        std::vector<Course::Coordinate> neighbours = coord.neighbours();
        for (auto n : neighbours) {
            if (n.x() < 0 || n.y() < 0 || n.x() > 9 || n.y() > 9) {
                continue;
            }
            tile = m_ObjManager->getTile(n);
            if (tile->getType() != "Water" && tile->getBuildingCount() == 0 && (tile->getWorkerCount() == 0 || tile->getOwner()->getName() == m_ObjManager->getPlayer(p_i)->getName())) {
                coord = tile->getCoordinate();
                break;
            }
        }
    }
    return coord;
}

Course::Coordinate MapWindow::getCoordinateForUnit(Course::Coordinate c, unsigned int p_i)
{
    auto tile = m_ObjManager->getTile(c);
    Course::Coordinate coord = tile->getCoordinate();
    // Checks if the tile contains water. Checks neighbour tiles.
    while (tile->getType() == "Water") {
        std::vector<Course::Coordinate> neighbours = coord.neighbours();
        for (auto n : neighbours) {
            auto tile = m_ObjManager->getTile(n);
            if (tile->getType() != "Water" && tile->getWorkerCount() == 0 &&
                    (tile->getBuildingCount() == 0 || tile->getOwner()->getName() == m_ObjManager->getPlayer(p_i)->getName())) {
                coord = tile->getCoordinate();
                break;
            }
        }
    }
    return coord;
}

bool MapWindow::checkIfValidCoordinateForBuilding(Course::Coordinate c, unsigned int p_i)
{
    auto tile = m_ObjManager->getTile(c);
    if (tile->getType() == "Water") {
        return false;
    } else if (tile->getBuildingCount() > 0) {
        return false;
    } else if (tile->getWorkerCount() > 0 && tile->getOwner()->getName() != m_ObjManager->getPlayer(p_i)->getName()) {
        return false;
    } else {
        return true;
    }
}

bool MapWindow::checkIfValidCoordinateForUnit(Course::Coordinate c, unsigned int p_i)
{
    auto tile = m_ObjManager->getTile(c);
    if (tile->getType() == "Water") {
        return false;
    } else if (tile->getWorkerCount() > 0) {
        return false;
    } else if (tile->getBuildingCount() > 0 && tile->getOwner()->getName() != m_ObjManager->getPlayer(p_i)->getName()) {
        return false;
    } else {
        return true;
    }
}

void MapWindow::on_endTurnB_clicked()
{
    qDebug() << QString("End turn clicked");
    try {
        m_GEHandler->addResources(player_index_, m_ObjManager->getPlayer(player_index_)->getObjects(), m_ObjManager);
    } catch (Course::InvalidPointer&) {
        qDebug("Adding resources failed!");
    }
    updateResourceMaps(player_index_);

    unsigned int i = 0;
    do {
        player_index_++; // increasing the player_index_ enables checkHQDefeat to assess the situation iteratively
        if (player_index_ == m_ObjManager->getPlayerCount())
        {
            player_index_ = 0;
            turn_++;
        }
        if (i==m_ObjManager->getPlayerCount()-1) {
            game_win_ = player_index_; // player_index_ should always be the remaining player
        }
        i++;
    } while (Student::checkHQDefeat(player_index_, m_ObjManager));

    QString end_turn_msg = QString::fromStdString("End turn clicked. Proceeding to player "+m_ObjManager->getPlayer(player_index_)->getName()+"\n(Turn "+std::to_string(turn_)+")");
    m_ui->textBrowser->setText(end_turn_msg);

    if (game_win_ != -1) {
        QString msg = QString::fromStdString("All hail "+m_ObjManager->getPlayer(game_win_)->getName()+", the unchallenged leader of Pirkanmaa!\n\n[Game over]");
        m_ui->textBrowser->setText(msg);
        m_ui->endTurnB->setDisabled(true);
    }
    updateIcons();
    updateTurnLabel();

    active_element_ = __func__;
}

void MapWindow::on_quitB_clicked()
{
    qDebug() << QString("Quit clicked");
    QApplication::quit();
}

void MapWindow::on_workerB_clicked()
{

    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";

        ss.append("Train a worker\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::WORKER_RECRUITMENT_COST);

        ss.append("\nClick a square where you want to train it. Then click Train");

        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Train");

        active_element_ = __func__;
    }
}

void MapWindow::on_spearmanB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";

        ss.append("Train a spearman\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::SPEARMAN_RECRUITMENT_COST);

        ss.append("\nClick a square where you want to train it. Then click Train");

        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Train");

        active_element_ = __func__;
    }
}

void MapWindow::on_archerB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";

        ss.append("Train an archer\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::ARCHER_RECRUITMENT_COST);

        ss.append("\nClick a square where you want to train it. Then click Train");

        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Train");

        active_element_ = __func__;
    }
}

void MapWindow::on_cavalryB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";

        ss.append("Train a cavalry unit\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::CAVALRY_RECRUITMENT_COST);

        ss.append("\nClick a square where you want to train it. Then click Train");

        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Train");

        active_element_ = __func__;
    }
}

void MapWindow::on_HQB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build a HQ\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::HQ_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}

void MapWindow::on_outpostB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build an Outpost\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::OUTPOST_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}

void MapWindow::on_farmB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build a Farm\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::FARM_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}

void MapWindow::on_mineB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build a Mine\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::MINE_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}


void MapWindow::on_quarryB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build a Quarry\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::QUARRY_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}

void MapWindow::on_sawmillB_clicked()
{
    if (active_element_ == __func__) {
        m_ui->textBrowser->setText("");
        m_ui->select_execB->setText("Select");
        active_element_ = "";
    } else {
        std::string ss = "";
        ss.append("Build a Sawmill\n\n");

        ss = uiTextResources(ss, Student::ConstResourceMaps::SAWMILL_BUILD_COST);

        ss.append("\nClick a square where you want to build it. Then click Build");
        QString worker_info = QString::fromStdString(ss);
        m_ui->textBrowser->setText(worker_info);
        m_ui->select_execB->setText("Build");

        active_element_ = __func__;
    }
}

std::string MapWindow::uiTextResources(std::string s, Student::ResourceMap cost_map) {
    for ( auto it = cost_map.begin(); it != cost_map.end(); it++ )
    {
        std::vector<std::string> res_names = {"Gold", "Food", "Wood", "Stone", "Ore"};
        if (it->first <= res_names.size()) {
            std::string resource = res_names.at(it->first - 1);
            std::string st = std::to_string(it->second);
            std::string item = resource + ": " + st + "\n";
            s.append(item);
        } else {
            // Execution should never get here
            qDebug() << QString::fromStdString(std::to_string(it->first));
        }
    }
    return s;
}

void MapWindow::on_select_execB_clicked()
{
    //qDebug() << QString::fromStdString(std::to_string(m_simplescene->getActiveElement()));
    std::string act_elem = std::to_string(m_simplescene->getActiveElement());

    Course::Coordinate c(0,0);
    if (act_elem.size() == 1) {
        c.set_x(0);
        c.set_y(std::stoi(act_elem));

    } else {
        c.set_x(std::stoi(act_elem.substr(0,1)));
        c.set_y(std::stoi(act_elem.substr(1,1)));
    }

    std::pair<std::string,std::string> coord;
    coord = std::make_pair(std::to_string(c.x()),std::to_string(c.y()));

    if (m_ui->select_execB->text() == "Select") {
        m_ui->select_execB->setText("Move / Attack");
        std::string browser_text = "Unit selected on (" + coord.first + "," + coord.second + ")\nMove or attack with the chosen unit by clicking adjacent square and Move / Attack";
        m_ui->textBrowser->setText(QString::fromStdString(browser_text));
        map_item_selected_ = m_simplescene->getActiveElement();

    } else if (m_ui->select_execB->text() == "Move / Attack"){

        m_ui->select_execB->setText("Select");

        Course::Coordinate c2 = Student::uintToCoord(map_item_selected_);
        bool in_neighbors = Student::checkIfNeighbors(c,c2);
        if (in_neighbors) {
            map_item_exec_ = m_simplescene->getActiveElement();

            // Calls move - function
            if (!moveSelectedUnit(player_index_)) {
                m_ui->textBrowser->setText("Illegal movement!");
            }

        } else {
            //qDebug() << QString::fromStdString(std::to_string(map_item_selected_))
            //         << QString::fromStdString(std::to_string(m_simplescene->getActiveElement()));
            m_ui->textBrowser->setText("Unit can only be moved to it's neighboring tiles!");
        }

        map_item_selected_ = 0;
        map_item_exec_ = 0;

    } else if (m_ui->select_execB->text() == "Build") {
        m_ui->select_execB->setText("Select");

        bool success = buildSelectedBuilding(c);
        active_element_ = "";

        if (success) {
            m_ui->textBrowser->setText("Building constructed.");
        }

    } else if (m_ui->select_execB->text() == "Train") {
        m_ui->select_execB->setText("Select");

        bool success = buildSelectedUnit(c);
        active_element_ = "";

        if (success) {
            m_ui->textBrowser->setText("Unit trained.");
        }
    }
}

bool MapWindow::buildSelectedBuilding(Course::Coordinate coord)
{
    if (active_element_ == "") {
        return false;
    } else {
        auto tile = m_ObjManager->getTile(coord);

        if (tile->getOwner() != nullptr && tile->getOwner()->getName() != m_ObjManager->getPlayer(player_index_)->getName()) {
            m_ui->textBrowser->setText("Can't build on a square already controlled by enemy!");
            return false;
        }

        auto building_type = returnBuildingType();
        if (building_type == nullptr) {
            m_ui->textBrowser->setText("No selected building to be constructed!");
            return false;
        }

        std::vector<Course::Coordinate> build_coords = Student::returnObjCoords("ALL_BUILDINGS",player_index_,m_ObjManager);
        bool next_to_building = false;
        for (auto c : build_coords) {
            if (Student::checkIfNeighbors(c, coord)) {
                next_to_building = true;
            }
        }
        if (next_to_building) {
            if (payForBuilding(building_type)) {
                if (m_ObjManager->addBuilding(building_type, tile, player_index_)) {
                    tile->setOwner(m_ObjManager->getPlayer(player_index_));
                    updateItem(tile);
                    m_ObjManager->getPlayer(player_index_)->addObject(building_type);
                    return true;
                } else {
                    returnPaymentBuilding(building_type);
                    m_ui->textBrowser->setText("Can't build there!");
                }
            } else {
                m_ui->textBrowser->setText("Not enough resources!");
            }
        } else {
            m_ui->textBrowser->setText("Buildings can only be built next to other buildings!");
        }
    } return false;
}

bool MapWindow::payForBuilding(std::shared_ptr<Course::BuildingBase> building)
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    bool success = false;
    auto b_name = building->getType();
    if (b_name == "HeadQuarters") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::HQ_BUILD_COST);
    } else if (b_name == "Outpost") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::OUTPOST_BUILD_COST);
    } else if (b_name == "Farm") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::FARM_BUILD_COST);
    } else if (b_name == "Sawmill") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::SAWMILL_BUILD_COST);
    } else if (b_name == "Quarry") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::QUARRY_BUILD_COST);
    } else if (b_name == "Mine") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::MINE_BUILD_COST);
    }
    if (success) {
        updateResourceMaps(player_index_);
        return true;
    } else {
        return false;
    }
}

void MapWindow::returnPaymentBuilding(std::shared_ptr<Course::BuildingBase> building) {
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    auto b_name = building->getType();
    if (b_name == "HeadQuarters") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::HQ_BUILD_COST);
    } else if (b_name == "Outpost") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::OUTPOST_BUILD_COST);
    } else if (b_name == "Farm") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::FARM_BUILD_COST);
    } else if (b_name == "Sawmill") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::SAWMILL_BUILD_COST);
    } else if (b_name == "Quarry") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::QUARRY_BUILD_COST);
    } else if (b_name == "Mine") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::MINE_BUILD_COST);
    }
    updateResourceMaps(player_index_);
}

std::shared_ptr<Course::BuildingBase> MapWindow::returnBuildingType()
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    if (active_element_ == "on_HQB_clicked") {
        return std::make_shared<Student::HeadQ>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_outpostB_clicked") {
        return std::make_shared<Student::Outpost>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_farmB_clicked") {
        return std::make_shared<Student::Farm>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_sawmillB_clicked") {
        return std::make_shared<Student::Sawmill>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_quarryB_clicked") {
        return std::make_shared<Student::Quarry>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_mineB_clicked") {
        return std::make_shared<Student::Mine>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else {
        return nullptr;
    }
}

bool MapWindow::buildSelectedUnit(Course::Coordinate coord)
{
    if (active_element_ == "") {
        return false;
    } else {
        auto tile = m_ObjManager->getTile(coord);

        if (tile->getOwner() != nullptr && tile->getOwner()->getName() != m_ObjManager->getPlayer(player_index_)->getName()) {
            m_ui->textBrowser->setText("Can't train on a square already controlled by enemy!");
            return false;
        }

        auto unit_type = returnUnitType();
        if (unit_type == nullptr) {
            m_ui->textBrowser->setText("No selected unit to be trained!");
            return false;
        }

        std::vector<Course::Coordinate> build_coords;
        if (unit_type->getType() == "Worker") {
            build_coords = Student::returnObjCoords("HeadQuarters",player_index_,m_ObjManager);
        } else {
            build_coords = Student::returnObjCoords("Outpost",player_index_,m_ObjManager);
        }
        bool next_to_building = false;
        for (auto c : build_coords) {
            if (Student::checkIfNeighbors(c, coord)) {
                next_to_building = true;
            }
        }
        if (next_to_building) {
            if (payForUnit(unit_type)) {
                if (m_ObjManager->addUnit(unit_type, tile, player_index_)) {
                    tile->setOwner(m_ObjManager->getPlayer(player_index_));
                    updateItem(tile);
                    m_ObjManager->getPlayer(player_index_)->addObject(unit_type);
                    return true;
                } else {
                    returnPaymentUnit(unit_type);
                    m_ui->textBrowser->setText("Units can't be trained there!");
                }
            } else {
                m_ui->textBrowser->setText("Not enough resources!");
            }
        } else {
            if (unit_type->getType() == "Worker") {
                m_ui->textBrowser->setText("Workers can only be built next to HQs!");
            } else {
                m_ui->textBrowser->setText("Military units can only be built next to Outposts!");
            }
        }
    } return false;
}

bool MapWindow::payForUnit(std::shared_ptr<Course::WorkerBase> unit)
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    bool success = false;
    auto u_name = unit->getType();
    if (u_name == "Worker") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::WORKER_RECRUITMENT_COST);
    } else if (u_name == "Spearman") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::SPEARMAN_RECRUITMENT_COST);
    } else if (u_name == "Archer") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::ARCHER_RECRUITMENT_COST);
    } else if (u_name == "Cavalry") {
        success = m_GEHandler->substractFromResourceMap(player_index_, Student::ConstResourceMaps::CAVALRY_RECRUITMENT_COST);
    }

    if (success) {
        updateResourceMaps(player_index_);
        return true;
    } else {
        return false;
    }
}

void MapWindow::returnPaymentUnit(std::shared_ptr<Course::WorkerBase> unit)
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    auto u_name = unit->getType();
    if (u_name == "Worker") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::WORKER_RECRUITMENT_COST);
    } else if (u_name == "Spearman") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::SPEARMAN_RECRUITMENT_COST);
    } else if (u_name == "Archer") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::ARCHER_RECRUITMENT_COST);
    } else if (u_name == "Cavalry") {
        m_GEHandler->addToResourceMap(player_index_, Student::ConstResourceMaps::CAVALRY_RECRUITMENT_COST);
    }
    updateResourceMaps(player_index_);
}

std::shared_ptr<Course::WorkerBase> MapWindow::returnUnitType()
{
    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    if (active_element_ == "on_workerB_clicked") {
        return std::make_shared<Student::Worker>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_spearmanB_clicked") {
        return std::make_shared<Student::Spearman>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_archerB_clicked") {
        return std::make_shared<Student::Archer>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else if (active_element_ == "on_cavalryB_clicked") {
        return std::make_shared<Student::Cavalry>(m_GEHandler, m_ObjManager, m_ObjManager->getPlayer(player_index_));
    } else {
        return nullptr;
    }
}

bool MapWindow::moveSelectedUnit(unsigned int p_i)
{
    Course::Coordinate c_from(0,0);
    Course::Coordinate c_to(0,0);

    c_from.set_x(map_item_selected_ / 10);
    c_from.set_y(map_item_selected_ % 10);

    c_to.set_x(map_item_exec_ / 10);
    c_to.set_y(map_item_exec_ % 10);

    auto tile_from = m_ObjManager->getTile(c_from);
    auto tile_to = m_ObjManager->getTile(c_to);

    // If tile has no unit or destination is not legal
    if ((tile_from->getWorkerCount() <= 0)
       || (tile_from->getOwner() == nullptr)
       || (tile_from->getOwner()->getName() != m_ObjManager->getPlayer(p_i)->getName())
       || (tile_to->getType() == "Water")) {
        return false;
    }
    auto unit = tile_from->getWorkers()[0];

    //qDebug() << QString::fromStdString(std::to_string(tile_from->getWorkerCount())+" worker count in from tile");
    //qDebug() << QString::fromStdString(std::to_string(tile_from->getBuildingCount())+" bld count in from tile");
    //qDebug() << QString::fromStdString(std::to_string(tile_to->getWorkerCount())+" worker count in to tile");
    //qDebug() << QString::fromStdString(std::to_string(tile_to->getBuildingCount())+" bld count in to tile");

    m_ObjManager->getPlayer(p_i)->removeObject(unit);
    tile_from->removeWorker(unit);

    if (tile_from->getBuildingCount() == 0) {
        // Remove ownership, if there is no buildings on tile
        tile_from->setOwner(nullptr);
    }
    if (tile_to->getOwner() == nullptr) {
        // Add ownership, if no owner
        tile_to->setOwner(m_ObjManager->getPlayer(p_i));
        tile_to->addWorker(unit);
        m_ObjManager->getPlayer(p_i)->addObject(unit);
        m_ui->textBrowser->setText("Unit moved.");
    } else if (tile_to->getOwner()->getName() != m_ObjManager->getPlayer(p_i)->getName()) {
        // Battle
        bool win = calculateBattleResults(unit, tile_to);
        if (win) {
            // while for safety reasons (if should be sufficient here)
            while (tile_to->getWorkerCount() > 0) {
                tile_to->removeWorker(tile_to->getWorkers()[0]);
            }
            // while for safety reasons (if should be sufficient here)
            while (tile_to->getBuildingCount() > 0) {
                // m_ObjManager->removeBuilding(tile_to->getBuildings()[0]->ID); // Buildings should also be removed like this, but this solution leads to a lot of problems.
                tile_to->removeBuilding(tile_to->getBuildings()[0]);
            }
            tile_to->setOwner(m_ObjManager->getPlayer(p_i));
            tile_to->addWorker(unit);
            m_ObjManager->getPlayer(p_i)->addObject(unit);
            m_ui->textBrowser->setText("Unit went into battle and won!");
        } else {
            m_ui->textBrowser->setText("Unit went into battle and lost!");
        }
    } else {
        if (tile_to->getWorkerCount() > 0) {
            // If own tile, but it has unit
            tile_from->addWorker(unit);
            m_ObjManager->getPlayer(p_i)->addObject(unit);
            tile_from->setOwner(m_ObjManager->getPlayer(p_i));
            m_ui->textBrowser->setText("Only one unit / square.");
        } else if (tile_to->getBuildingCount() > 0) {
            // If already own tile with building
            tile_to->addWorker(unit);
            m_ObjManager->getPlayer(p_i)->addObject(unit);
            tile_from->setOwner(m_ObjManager->getPlayer(p_i));
        }
    }

    updateItem(tile_from);
    updateItem(tile_to);

    //qDebug() << QString::fromStdString(std::to_string(tile_from->getWorkerCount())+" worker count in from tile");
    //qDebug() << QString::fromStdString(std::to_string(tile_to->getWorkerCount())+" worker count in to tile");

    return true;

}

bool MapWindow::calculateBattleResults(std::shared_ptr<Course::WorkerBase> unit, std::shared_ptr<Course::TileBase> tile)
{
    //double attack_value = ((double) rand() / (RAND_MAX));
    double attack_value = m_rnd_dist(m_rnd_engine);

    // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
    Student::CombatMap fighter_unit;
    if (unit->getType() == "Worker") {
        fighter_unit = Student::CombatMaps::WORKER_CM;
    } else if (unit->getType() == "Spearman") {
        fighter_unit = Student::CombatMaps::SPEARMAN_CM;
    } else if (unit->getType() == "Archer") {
        fighter_unit = Student::CombatMaps::ARCHER_CM;
    } else if (unit->getType() == "Cavalry") {
        fighter_unit = Student::CombatMaps::CAVALRY_CM;
    }

    std::shared_ptr<Course::WorkerBase> opponentU = nullptr;
    std::shared_ptr<Course::BuildingBase> opponentB = nullptr;

    if (tile->getWorkerCount() > 0) {
        opponentU = tile->getWorkers()[0];
    }
    if (tile->getBuildingCount() > 0) {
        opponentB = tile->getBuildings()[0];
    }

    if (opponentU != nullptr) {

        unsigned int oppU;
        if (opponentU->getType() == "Worker") {
            oppU = 10;
        } else if (opponentU->getType() == "Spearman") {
            oppU = 11;
        } else if (opponentU->getType() == "Cavalry") {
            oppU = 12;
        } else if (opponentU->getType() == "Archer") {
            oppU = 13;
        }

        for ( auto it = fighter_unit.begin(); it != fighter_unit.end(); it++ )
        {
            if (it->first == oppU) {
                //qDebug() << QString::fromStdString(std::to_string(it->second));
                //qDebug() << QString::fromStdString(std::to_string(attack_value));
                if (it->second < attack_value) {
                    return false;
                }
            }
        }
    }

    if (opponentB != nullptr) {

        // FUTURE DEVELOPMENT NOTE there should be more clean lambda map solution
        unsigned int oppB;
        if (opponentB->getType() == "HeadQuarters") {
            oppB = 1;
        } else if (opponentB->getType() == "Outpost") {
            oppB = 2;
        } else if (opponentB->getType() == "Farm") {
            oppB = 3;
        } else if (opponentB->getType() == "Mine") {
            oppB = 4;
        } else if (opponentB->getType() == "Quarry") {
            oppB = 5;
        } else if (opponentB->getType() == "Sawmill") {
            oppB = 6;
        }

        for ( auto it = fighter_unit.begin(); it != fighter_unit.end(); it++ )
        {
            if (it->first == oppB) {
                //qDebug() << QString::fromStdString(std::to_string(it->second));
                //qDebug() << QString::fromStdString(std::to_string(attack_value));
                if (it->second < attack_value) {
                    return false;
                }
            }
        }
    }
    return true;
}
