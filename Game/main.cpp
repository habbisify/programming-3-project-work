#include "settingswindow.h"
#include "mapwindow.hh"

#include <QApplication>


int main(int argc, char* argv[])
{

    QApplication app(argc, argv);

    SettingsWindow settingsWindow;

    MapWindow mapWindow;
    mapWindow.showMinimized();

    settingsWindow.show();

    QObject::connect(&settingsWindow, &SettingsWindow::send_data, &mapWindow, &MapWindow::import_data);

    return app.exec();
}
