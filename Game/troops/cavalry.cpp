#include "cavalry.h"

namespace Student {

Cavalry::Cavalry(const std::shared_ptr<GameEventHandler>& eventhandler,
                         const std::shared_ptr<ObjectManager>& objectmanager,
                         const std::shared_ptr<Student::PlayerC>& owner,
                         const int& tilespaces,
                         const ResourceMap& cost,
                         const ResourceMapDouble& efficiency,
                         const CombatMap& combat):
    WorkerBase(
        eventhandler,
        objectmanager,
        owner,
        tilespaces,
        cost,
        efficiency)
{auto combat_ = combat;
}

std::string Cavalry::getType() const
{
    return "Cavalry";
}

bool Cavalry::canBePlacedOnTile(const std::shared_ptr<TileBase> &target) const
{
    return WorkerBase::canBePlacedOnTile(target);
}

void Cavalry::doSpecialAction()
{

}

const ResourceMapDouble Cavalry::tileWorkAction()
{
    return {};
}

unsigned int Cavalry::getMovement() {
    return movement_;
}
void Cavalry::setMovement(unsigned int movement) {
    movement_ = movement;
}

} // namespace Student
