#pragma once

#include "workers/workerbase.h"
#include "gameeventhandler.h"
#include "objectmanager.h"
#include "logic/playerC.h"
#include "core_st/resourcemaps.h"
#include "core_st/combatmaps.h"
#include "core/playerbase.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource;
using Course::TileBase;
using Student::GameEventHandler;
using Student::ObjectManager;

class Archer : public Course::WorkerBase
{
public:

    /**
     * @brief Disabled parameterless constructor.
     */
    Archer() = delete;

    Archer(const std::shared_ptr<GameEventHandler>& eventhandler,
               const std::shared_ptr<ObjectManager>& objectmanager,
               const Player& owner,
               const int& tilespaces = 1,
               const ResourceMap& cost = ConstResourceMaps::ARCHER_RECRUITMENT_COST,
               const ResourceMapDouble& efficiency = {},
               const CombatMap& combat = CombatMaps::ARCHER_CM);

    /**
     * @brief Default destructor.
     */
    virtual ~Archer() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;

    /**
     * @brief Default placement rule for workers:\n
     * * Tile must have space for the worker
     * * Tile must have same owner as the worker
     * @param target is the Tile that worker is being placed on.
     * @return
     * True - Only if both conditions are met.
     * @post Exception guarantee: Basic
     * @note
     * Override to change placement rules for derived worker.
     */
    virtual bool canBePlacedOnTile(
            const std::shared_ptr<TileBase> &target) const override;

    /**
     * @brief Performs the Worker's special action. (If any)
     *
     * @note Hint: You can use game-eventhandler for more creative solutions.
     */
    virtual void doSpecialAction();

    /**
     * @brief Returns Worker's efficiency at resource production.
     * Worker consumes FOOD and MONEY. Resource consumption and resource
     * focus determine final multiplier that is based on WORKER_EFFICIENCY.
     *
     * @return
     */
    virtual const ResourceMapDouble tileWorkAction() override;

    unsigned int getMovement();
    void setMovement(unsigned int movement);
private:
    std::shared_ptr<CombatMap> combat_ = {};
    unsigned int movement_ = 1;


}; // class Archer

} // namespace Student
