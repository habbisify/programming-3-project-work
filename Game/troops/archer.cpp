#include "archer.h"

namespace Student {

Archer::Archer(const std::shared_ptr<GameEventHandler>& eventhandler,
                         const std::shared_ptr<ObjectManager>& objectmanager,
                         const std::shared_ptr<Student::PlayerC>& owner,
                         const int& tilespaces,
                         const ResourceMap& cost,
                         const ResourceMapDouble& efficiency,
                         const CombatMap& combat):
    WorkerBase(
        eventhandler,
        objectmanager,
        owner,
        tilespaces,
        cost,
        efficiency)
{auto combat_ = combat;
}

std::string Archer::getType() const
{
    return "Archer";
}

bool Archer::canBePlacedOnTile(const std::shared_ptr<TileBase> &target) const
{
    return WorkerBase::canBePlacedOnTile(target);
}

void Archer::doSpecialAction()
{

}

const ResourceMapDouble Archer::tileWorkAction()
{
    return {};
}

unsigned int Archer::getMovement() {
    return movement_;
}
void Archer::setMovement(unsigned int movement) {
    movement_ = movement;
}

} // namespace Student

