#include "spearman.h"

namespace Student {

Spearman::Spearman(const std::shared_ptr<GameEventHandler>& eventhandler,
                         const std::shared_ptr<ObjectManager>& objectmanager,
                         const std::shared_ptr<Student::PlayerC>& owner,
                         const int& tilespaces,
                         const ResourceMap& cost,
                         const ResourceMapDouble& efficiency,
                         const CombatMap& combat):
    WorkerBase(
        eventhandler,
        objectmanager,
        owner,
        tilespaces,
        cost,
        efficiency)
{auto combat_ = combat;
}

std::string Spearman::getType() const
{
    return "Spearman";
}

bool Spearman::canBePlacedOnTile(const std::shared_ptr<TileBase> &target) const
{
    return WorkerBase::canBePlacedOnTile(target);
}

void Spearman::doSpecialAction()
{

}

const ResourceMapDouble Spearman::tileWorkAction()
{
    return {};
}

unsigned int Spearman::getMovement() {
    return movement_;
}
void Spearman::setMovement(unsigned int movement) {
    movement_ = movement;
}

} // namespace Student
