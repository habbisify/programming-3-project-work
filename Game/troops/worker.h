#pragma once

#include "workers/workerbase.h"
#include "gameeventhandler.h"
#include "objectmanager.h"
#include "logic/playerC.h"
#include "core_st/resourcemaps.h"
#include "core_st/combatmaps.h"
#include "core/playerbase.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource;
using Course::TileBase;
using Student::GameEventHandler;
using Student::ObjectManager;

class Worker : public Course::WorkerBase
{
public:
    /**
     * @brief Disabled parameterless constructor.
     */
    Worker() = delete;

    /**
     * @brief Constructor for the class.
     *
     * @param eventhandler points to the student's GameEventHandler.
     * @param owner points to the owning player.
     * @param descriptions contains descriptions and flavor texts.
     */
    Worker(const std::shared_ptr<GameEventHandler>& eventhandler,
                const std::shared_ptr<ObjectManager>& objectmanager,
                const Player& owner,
                const int& tilespaces = 1,
                const ResourceMap& cost =
                    ConstResourceMaps::WORKER_RECRUITMENT_COST,
                const ResourceMapDouble& efficiency =
                    ConstResourceMaps::WORKER_EFFICIENCY,
                const CombatMap& combat = CombatMaps::WORKER_CM
                );

    /**
     * @brief Default destructor.
     */
    virtual ~Worker() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;

    /**
     * @brief Check if the worker can be placed on the Tile according to
     * it's placement rule. Only rule is that the Tile must have same owner
     * as the worker.
     * @param target is the Tile that worker is being placed on.
     * @return
     * True - If baseclass' method return true and target Tile has space
     * for worker.
     * False - If both conditions aren't met.
     * @note Override to change placement rules for derived worker.
     * @post Exception guarantee: Basic
     */
    virtual bool canBePlacedOnTile(
            const std::shared_ptr<TileBase> &target) const override;

    /**
     * @brief Performs the Worker's default action.
     */
    virtual void doSpecialAction() override;

    /**
     * @brief Returns Worker's efficiency at resource production.
     * @return
     */
    virtual const ResourceMapDouble tileWorkAction() override;

    unsigned int getMovement();
    void setMovement(unsigned int movement);
private:
    std::shared_ptr<CombatMap> combat_ = {};
    unsigned int movement_ = 1;

}; // class Worker
} // namespace Student
