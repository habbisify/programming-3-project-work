#include "worker.h"

namespace Student {

Worker::Worker(const std::shared_ptr<GameEventHandler>& eventhandler,
                         const std::shared_ptr<ObjectManager>& objectmanager,
                         const std::shared_ptr<Student::PlayerC>& owner,
                         const int& tilespaces,
                         const ResourceMap& cost,
                         const ResourceMapDouble& efficiency,
                         const CombatMap& combat):
    WorkerBase(
        eventhandler,
        objectmanager,
        owner,
        tilespaces,
        cost,
        efficiency)
{auto combat_ = combat;
}

std::string Worker::getType() const
{
    return "Worker";
}

bool Worker::canBePlacedOnTile(const std::shared_ptr<TileBase> &target) const
{
    return WorkerBase::canBePlacedOnTile(target);
}

void Worker::doSpecialAction()
{

}

const ResourceMapDouble Worker::tileWorkAction()
{
    auto player = getOwner();
    auto events = lockEventHandler();

    ResourceMapDouble final_modifier;

    for( auto it = WORKER_EFFICIENCY.begin();
         it != WORKER_EFFICIENCY.end();
         ++it )
    {
        final_modifier[it->first] = it->second;
    }

    return final_modifier;
}

unsigned int Worker::getMovement() {
    return movement_;
}
void Worker::setMovement(unsigned int movement) {
    movement_ = movement;
}

} // namespace Student
