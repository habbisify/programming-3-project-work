#pragma once

#include "gameeventhandler.h"
#include "objectmanager.h"
#include "buildings/buildingbase.h"
#include "core/resourcemaps.h"
#include "core/playerbase.h"
#include "core_st/resourcemaps.h"
#include "logic/playerC.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource;
using Course::TileBase;
using Student::GameEventHandler;
using Student::ObjectManager;

/**
 * @brief The Outpost class represents a Outpost-building in the game.
 *
 * The Outpost can create military units
 */
class Outpost : public Course::BuildingBase
{
public:
    /**
     * @brief Disabled parameterless constructor.
     */
    Outpost() = delete;

    /**
     * @brief Constructor for the class.
     *
     * @param eventhandler points to the student's GameEventHandler.
     * @param owner points to the owning player.
     * @param tile points to the tile upon which the building is constructed.
     *
     * @post Exception Guarantee: No guarantee.
     * @exception OwnerConflict - if the building conflicts with tile's
     * ownership.
     */
    explicit Outpost(
            const std::shared_ptr<GameEventHandler>& eventhandler,
            const std::shared_ptr<ObjectManager>& objectmanager,
            const std::shared_ptr<Student::PlayerC>& owner,
            const int& tilespaces = 1,
            const ResourceMap& buildcost = ConstResourceMaps::OUTPOST_BUILD_COST,
            const ResourceMap& production = ConstResourceMaps::OUTPOST_PRODUCTION
            );

    /**
     * @brief Default destructor.
     */
    virtual ~Outpost() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;

}; // class Outpost

} // namespace Student
