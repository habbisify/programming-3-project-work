#include "farm.h"

namespace Student {

Farm::Farm(const std::shared_ptr<GameEventHandler>& eventhandler,
           const std::shared_ptr<ObjectManager>& objectmanager,
           const std::shared_ptr<Student::PlayerC>& owner,
           const int& tilespaces,
           const ResourceMap& buildcost,
           const ResourceMap& production
           ):
    BuildingBase(
        eventhandler,
        objectmanager,
        owner,
        tilespaces,
        buildcost,
        production
        )
{
}

std::string Farm::getType() const
{
    return "Farm";
}

} // namespace Student
