#pragma once

#include "gameeventhandler.h"
#include "objectmanager.h"
#include "buildings/buildingbase.h"
#include "core/resourcemaps.h"
#include "core/playerbase.h"
#include "core_st/resourcemaps.h"
#include "logic/playerC.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource;
using Course::TileBase;
using Student::GameEventHandler;
using Student::ObjectManager;

/**
 * @brief The Sawmill class represents a Sawmill-building in the game.
 *
 * The Sawmill doubles the pprduction of wood.
 */
class Sawmill : public Course::BuildingBase
{
public:
    /**
     * @brief Disabled parameterless constructor.
     */
    Sawmill() = delete;

    /**
     * @brief Constructor for the class.
     *
     * @param eventhandler points to the student's GameEventHandler.
     * @param owner points to the owning player.
     * @param tile points to the tile upon which the building is constructed.
     *
     * @post Exception Guarantee: No guarantee.
     * @exception OwnerConflict - if the building conflicts with tile's
     * ownership.
     */
    explicit Sawmill(
            const std::shared_ptr<GameEventHandler>& eventhandler,
            const std::shared_ptr<ObjectManager>& objectmanager,
            const std::shared_ptr<Student::PlayerC>& owner,
            const int& tilespaces = 1,
            const ResourceMap& buildcost = ConstResourceMaps::SAWMILL_BUILD_COST,
            const ResourceMap& production = ConstResourceMaps::SAWMILL_PRODUCTION_FOREST
            );

    /**
     * @brief Default destructor.
     */
    virtual ~Sawmill() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;


}; // class Sawmill

} // namespace Student
