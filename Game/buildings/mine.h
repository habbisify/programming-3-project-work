#pragma once

#include "gameeventhandler.h"
#include "objectmanager.h"
#include "buildings/buildingbase.h"
#include "core/resourcemaps.h"
#include "core/playerbase.h"
#include "core_st/resourcemaps.h"
#include "logic/playerC.h"

namespace Student {
using Course::ResourceMap;
using Course::ResourceMapDouble;
using Course::BasicResource;
using Course::TileBase;
using Student::GameEventHandler;
using Student::ObjectManager;

/**
 * @brief The Mine class represents a Mine-building in the game.
 *
 * The Mine doubles the production of ore.
 */
class Mine : public Course::BuildingBase
{
public:
    /**
     * @brief Disabled parameterless constructor.
     */
    Mine() = delete;

    /**
     * @brief Constructor for the class.
     *
     * @param eventhandler points to the student's GameEventHandler.
     * @param owner points to the owning player.
     * @param tile points to the tile upon which the building is constructed.
     *
     * @post Exception Guarantee: No guarantee.
     * @exception OwnerConflict - if the building conflicts with tile's
     * ownership.
     */
    explicit Mine(
            const std::shared_ptr<GameEventHandler>& eventhandler,
            const std::shared_ptr<ObjectManager>& objectmanager,
            const std::shared_ptr<Student::PlayerC>& owner,
            const int& tilespaces = 1,
            const ResourceMap& buildcost = ConstResourceMaps::MINE_BUILD_COST,
            const ResourceMap& production = ConstResourceMaps::MINE_PRODUCTION_MOUNTAIN
            );

    /**
     * @brief Default destructor.
     */
    virtual ~Mine() = default;

    /**
     * @copydoc GameObject::getType()
     */
    virtual std::string getType() const override;

}; // class Mine

} // namespace Student
