#ifndef TST_GAME_TEST_H
#define TST_GAME_TEST_H

#include <QtTest>
#include <QObject>
#include <QApplication>

#include <gameeventhandler.h>
#include <objectmanager.h>
#include <data_struct.h>
#include <buildings/hq.h>
#include <troops/worker.h>
#include <buildings/outpost.h>
// This allows access to private UI variables
#define protected public
#define private public
#include <mapwindow.hh>
#undef protected
#undef private


class Test_Game : public QObject
{
    Q_OBJECT

public:
    explicit Test_Game(QObject *parent = 0);
    ~Test_Game();

private slots:

    // Map Window
    void test_mapUI();
    void test_obj_man();
    void test_game_event_hand();
    void test_tile_creation();
    void test_tile_removal();
    void test_tile_update();
    void test_unit_creation();
    void building_creation();
    void test_unit_movement();
    void test_battle_system();
    void test_resources();
    void test_game_initialization();
    void test_updateTurnLabel();
};

#endif // TST_GAME_TEST_H
