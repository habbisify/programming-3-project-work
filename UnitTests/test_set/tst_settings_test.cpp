#include "tst_settings_test.h"

Test_Settings::Test_Settings(QObject *parent) : QObject(parent)
{

}

Test_Settings::~Test_Settings()
{

}

void Test_Settings::test_settingsUI()
{
    SettingsWindow s;

    s.show();
    QCOMPARE(s.isHidden(), false);
    s.hide();
    QCOMPARE(s.isHidden(), true);
}

void Test_Settings::test_name_changed_in_settings()
{
    SettingsWindow s;
    s.set_player_names();
    std::vector<std::string> orig_names = {"Greed", "Wrath", "Pride", "Sloth"};
    for (unsigned int i = 0; i < 4; i++)
    {
        QCOMPARE(s.players[i].first, orig_names.at(i));
    }

    std::vector<std::string> new_names = {"Gluttony", "Envy", "Lust", "7 Deadly Sins"};
    s.ui->p1name->setPlainText(QString::fromStdString(new_names.at(0)));
    s.ui->p2name->setPlainText(QString::fromStdString(new_names.at(1)));
    s.ui->p3name->setPlainText(QString::fromStdString(new_names.at(2)));
    s.ui->p4name->setPlainText(QString::fromStdString(new_names.at(3)));

    for (unsigned int i = 0; i < 4; i++)
    {
        QCOMPARE(s.players[i].first, new_names.at(i));
    }

}

void Test_Settings::test_check_player_status()
{
    SettingsWindow s(new QWidget);

    QCOMPARE(s.check_player_status(1), true);
    QCOMPARE(s.check_player_status(2), false);

    s.ui->player_n->setValue(3);
    QCOMPARE(s.check_player_status(2), true);
    QCOMPARE(s.check_player_status(3), false);

    s.ui->player_n->setValue(4);
    QCOMPARE(s.check_player_status(3), true);

}

void Test_Settings::test_on_player_n_valueChanged()
{
    SettingsWindow s(new QWidget);

    s.ui->player_n->setValue(1);
    QCOMPARE(s.check_player_status(2), false);
    s.ui->player_n->setValue(3);
    s.on_player_n_valueChanged(3);
    QCOMPARE(s.check_player_status(2), true);
    s.on_player_n_valueChanged(4);
    QCOMPARE(s.check_player_status(3), false);
    s.ui->player_n->setValue(4);
    QCOMPARE(s.check_player_status(3), true);
}

void Test_Settings::test_on_bot_n_valueChanged()
{
    SettingsWindow s(new QWidget);

    s.ui->player_n->setValue(3);
    QCOMPARE(s.check_player_status(0), true);
    QCOMPARE(s.check_player_status(1), true);
    QCOMPARE(s.check_player_status(2), true);
    QCOMPARE(s.check_player_status(3), false);
    s.ui->bot_n->setValue(2);
    QCOMPARE(s.check_player_status(2), false);
}

void Test_Settings::test_on_play_clicked()
{
    SettingsWindow s(new QWidget);
    s.show();
    s.on_playB_clicked();
    QCOMPARE(s.settings_created, true);
    QCOMPARE(s.isHidden(), true);
}

void Test_Settings::test_send_data()
{
    SettingsWindow s(new QWidget);
    s.ui->player_n->setValue(1);
    QCOMPARE(s.players.at(1).second, false);

    data_struct export_data;
    for (int i = 0; i < s.ui->bot_n->value() + s.ui->player_n->value(); ++i) {
        export_data.players.push_back(s.players[i]);
    }

    try {

    export_data.map_seed = s.ui->seed_n->value();
    export_data.chosen_preset = s.ui->tile_preset->currentIndex();
    export_data.win_condition = s.ui->win_preset->currentIndex();

    s.send_data(export_data);
    s.hide();
    QVERIFY(s.isHidden());
    } catch (...) {
        QFAIL("Data cannot be sent.");
    }
}

//#include "tst_settings_test.moc"
