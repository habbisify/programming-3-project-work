#ifndef TST_SETTINGS_TEST_H
#define TST_SETTINGS_TEST_H

#include <QtTest>
#include <QObject>
#include <QApplication>
#include <data_struct.h>

// This allows access to private UI variables
#define protected public
#define private public
#include <settingswindow.h>
#undef protected
#undef private


class Test_Settings: public QObject
{
    Q_OBJECT

public:
    explicit Test_Settings(QObject *parent = 0);
    ~Test_Settings();

private slots:
    // Settings Window
    void test_settingsUI();
    void test_name_changed_in_settings();
    void test_check_player_status();
    void test_on_player_n_valueChanged();
    void test_on_bot_n_valueChanged();
    void test_on_play_clicked();
    void test_send_data();

};

#endif // TST_SETTINGS_TEST_H
