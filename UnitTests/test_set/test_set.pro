QT += testlib

QT += core gui widgets

TARGET = maintest

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle
CONFIG += c++14

TEMPLATE = app

SOURCES +=  \
    tst_settings_test.cpp \
    tst_game_test.cpp \
    tst_course_test.cpp \
    tst_main_test.cpp \
    $$PWD/../../Game/mapwindow.cc \
    $$PWD/../../Game/objectmanager.cc \
    $$PWD/../../Game/gameeventhandler.cc \
    $$PWD/../../Game/settingswindow.cc \
    $$PWD/../../Game/til/forest.cpp \
    $$PWD/../../Game/til/grassland.cpp \
    $$PWD/../../Game/graphics/gamescene.cpp \
    $$PWD/../../Game/graphics/mapitem.cpp \
    $$PWD/../../Game/til/mountain.cpp \
    $$PWD/../../Game/til/rocks.cpp \
    $$PWD/../../Game/til/water.cpp \
    $$PWD/../../Game/troops/spearman.cpp \
    $$PWD/../../Game/troops/cavalry.cpp \
    $$PWD/../../Game/troops/archer.cpp \
    $$PWD/../../Game/buildings/mine.cpp \
    $$PWD/../../Game/buildings/quarry.cpp \
    $$PWD/../../Game/buildings/sawmill.cpp \
    $$PWD/../../Game/til/field.cpp \
    $$PWD/../../Game/logic/utilities.cpp \
    $$PWD/../../Game/logic/playerC.cpp \
    $$PWD/../../Game/troops/worker.cpp \
    $$PWD/../../Game/buildings/hq.cpp \
    $$PWD/../../Game/buildings/outpost.cpp \
    $$PWD/../../Game/buildings/farm.cpp


HEADERS += \
    tst_settings_test.h \
    tst_game_test.h \
    tst_course_test.h \
    $$PWD/../../Game/mapwindow.hh \
    $$PWD/../../Game/objectmanager.h \
    $$PWD/../../Game/settingswindow.h \
    $$PWD/../../Game/gameeventhandler.h \
    $$PWD/../../Game/til/forest.h \
    $$PWD/../../Game/til/grassland.h \
    $$PWD/../../Game/graphics/gamescene.h \
    $$PWD/../../Game/graphics/mapitem.h \
    $$PWD/../../Game/til/mountain.h \
    $$PWD/../../Game/core_st/resourcemaps.h \
    $$PWD/../../Game/core_st/combatmaps.h \
    $$PWD/../../Game/til/rocks.h \
    $$PWD/../../Game/til/water.h \
    $$PWD/../../Game/troops/spearman.h \
    $$PWD/../../Game/troops/cavalry.h \
    $$PWD/../../Game/troops/archer.h \
    $$PWD/../../Game/buildings/mine.h \
    $$PWD/../../Game/buildings/quarry.h \
    $$PWD/../../Game/buildings/sawmill.h \
    $$PWD/../../Game/til/field.h \
    $$PWD/../../Game/logic/utilities.h \
    $$PWD/../../Game/data_struct.h \
    $$PWD/../../Game/logic/playerC.h \
    $$PWD/../../Game/troops/worker.h \
    $$PWD/../../Game/buildings/hq.h \
    $$PWD/../../Game/buildings/outpost.h \
    $$PWD/../../Game/buildings/farm.h \
    $$PWD/../../Course/CourseLib/interfaces/igameeventhandler.h \
    $$PWD/../../Course/CourseLib/interfaces/iobjectmanager.h \
    $$PWD/../../Course/CourseLib/core/worldgenerator.h


FORMS += \
    $$PWD/../../Game/mapwindow.ui \
    $$PWD/../../Game/settingswindow.ui

RESOURCES += \
    $$PWD/../../Game/imgs/resources.qrc

INCLUDEPATH += $$PWD/../../Game \
                $$PWD/../../Course/CourseLib
DEPENDPATH += $$PWD/../../Game \
                $$PWD/../../Course/CourseLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Course/CourseLib/release/libCourseLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Course/CourseLib/debug/libCourseLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Course/CourseLib/release/CourseLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Course/CourseLib/debug/CourseLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../Course/CourseLib/libCourseLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Course/CourseLib/release/ -lCourseLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Course/CourseLib/debug/ -lCourseLib
else:unix: LIBS += -L$$OUT_PWD/../../Course/CourseLib/ -lCourseLib


