#ifndef TST_COURSE_TEST_H
#define TST_COURSE_TEST_H

#include <QtTest>
#include <QObject>

#include "interfaces/igameeventhandler.h"
#include "interfaces/iobjectmanager.h"
#include "core/worldgenerator.h"
#include "gameeventhandler.h"
#include "objectmanager.h"
#include "data_struct.h"
#include "til/forest.h"
#include "til/grassland.h"


class Test_Course : public QObject
{
    Q_OBJECT

public:
    explicit Test_Course(QObject *parent = 0);
    ~Test_Course();


private slots:
    // Course
    void test_igameobject_manager();
    void test_igameevent_handler();
    void test_modifyResource();
    void test_modifyResources();
    void test_getTiles();
    void test_getTile();
    void test_addTile();
    void test_generate_world();

};


#endif // TST_COURSE_TEST_H
