#include "tst_course_test.h"

Test_Course::Test_Course(QObject *parent) : QObject(parent)
{

}

Test_Course::~Test_Course()
{

}

void Test_Course::test_igameobject_manager()
{
    try {
        auto obj_man = std::make_shared<Student::ObjectManager>();
    } catch (...) {
        QFAIL("Object manager cannot be created.");
    }
}

void Test_Course::test_igameevent_handler()
{
    try {
        auto geh = std::make_shared<Student::GameEventHandler>();
    } catch (...) {
        QFAIL("Object manager cannot be created.");
    }

}

void Test_Course::test_modifyResource()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    data_struct ds;
    ds.players.push_back(std::make_pair<std::string,bool>("P1",1));
    ds.players.push_back(std::make_pair<std::string,bool>("P2",1));
    auto res_map = Student::ConstResourceMaps::INIT_RES;
    obj_man->setGameConf(ds);
    obj_man->addPlayers();
    geh->initializeResources(obj_man->getPlayerCount());
    try {
        geh->addToResourceMap(0, res_map);
        auto res = geh->getResources(0);
        QCOMPARE(res.at(0), 1000);
        geh->substractFromResourceMap(0, res_map);
        auto res2 = geh->getResources(0);
        QCOMPARE(res2.at(1), 500);
    } catch (...) {
        QFAIL("Resources cannot be added");
    }
}

void Test_Course::test_modifyResources()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    data_struct ds;
    ds.players.push_back(std::make_pair<std::string,bool>("P1",1));
    ds.players.push_back(std::make_pair<std::string,bool>("P2",1));
    //auto res_f = Course::BasicResource::FOOD;
    //auto res_map = Student::ConstResourceMaps::INIT_RES;
    obj_man->setGameConf(ds);
    obj_man->addPlayers();
    geh->initializeResources(obj_man->getPlayerCount());
    try {
        auto res = geh->getResources(0);
        QCOMPARE(res[0], 500);
    } catch (...) {
        QFAIL("Resources cannot be added");
    }
}

void Test_Course::test_getTiles()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    try {
        Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
        gen.addConstructor<Student::Forest>(10);
        gen.addConstructor<Student::Grassland>(20);
        gen.generateMap(10,10,0,obj_man,geh);
        QVERIFY(true);
    } catch (...) {
        QFAIL("Generate world failed");
    }
    std::vector<Course::Coordinate> coords = {Course::Coordinate(0,0), Course::Coordinate(0,1)};
    try {
        obj_man->getTiles(coords);
    } catch (...) {
        QFAIL("Error on getTiles");
    }
}

void Test_Course::test_getTile()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    try {
        Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
        gen.addConstructor<Student::Forest>(10);
        gen.addConstructor<Student::Grassland>(20);
        gen.generateMap(10,10,0,obj_man,geh);
        QVERIFY(true);
    } catch (...) {
        QFAIL("Generate world failed");
    }
    Course::Coordinate coord(0,0);
    try {
        obj_man->getTile(coord);
    } catch (...) {
        QFAIL("Error on getTile");
    }
}

void Test_Course::test_addTile()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    try {
        Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
        gen.addConstructor<Student::Forest>(10);
        gen.addConstructor<Student::Grassland>(20);
        gen.generateMap(10,10,0,obj_man,geh);
        QVERIFY(true);
    } catch (...) {
        QFAIL("addTiles failed in world creation");
    }
}

void Test_Course::test_generate_world()
{
    auto geh = std::make_shared<Student::GameEventHandler>();
    auto obj_man = std::make_shared<Student::ObjectManager>();
    try {
        Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
        gen.addConstructor<Student::Forest>(10);
        gen.addConstructor<Student::Grassland>(20);
        gen.generateMap(10,10,0,obj_man,geh);
        QVERIFY(true);
    } catch (...) {
        QFAIL("Generate world failed");
    }
}

//#include "tst_course_test.moc"
