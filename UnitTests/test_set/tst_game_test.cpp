#include "tst_game_test.h"



Test_Game::Test_Game(QObject *parent) : QObject(parent)
{

}

Test_Game::~Test_Game()
{

}

void Test_Game::test_mapUI()
{
    MapWindow m;

    m.show();
    QCOMPARE(m.isHidden(), false);
    m.hide();
    QCOMPARE(m.isHidden(), true);
}

void Test_Game::test_obj_man()
{
    Student::ObjectManager o;
    data_struct ds;
    ds.players = {};
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    ds.win_condition = 0;

    o.setGameConf(ds);
    QCOMPARE(o.getPlayerCount(), 0u);
    QCOMPARE(o.getSeed(), 2);
}

void Test_Game::test_game_event_hand()
{
    Student::GameEventHandler geh;
    geh.initializeResources(2);
    QCOMPARE(geh.getResources(1), geh.getResources(0));

    geh.initializeResources(4);
    QCOMPARE(geh.getResources(2), geh.getResources(1));
}

void Test_Game::test_tile_creation()
{
    MapWindow m;
    std::shared_ptr<Course::TileBase> obj = m.m_ObjManager->getTile(Course::Coordinate(0, 0));
    QVERIFY(obj == nullptr);
    Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
    m.constructMap(&gen);
    const int seed = m.m_ObjManager->getSeed();
    gen.generateMap(10,10,seed,m.m_ObjManager,m.m_GEHandler);

    std::shared_ptr<Course::TileBase> obj2 = m.m_ObjManager->getTile(Course::Coordinate(0, 0));
    QVERIFY(obj2 != nullptr);
}

void Test_Game::test_tile_removal()
{
    MapWindow m;
    std::shared_ptr<Course::TileBase> obj = m.m_ObjManager->getTile(Course::Coordinate(0, 0));
    QVERIFY(obj == nullptr);
    Course::WorldGenerator &gen = Course::WorldGenerator::getInstance();
    m.constructMap(&gen);
    const int seed = m.m_ObjManager->getSeed();
    gen.generateMap(10,10,seed,m.m_ObjManager,m.m_GEHandler);

    std::shared_ptr<Course::TileBase> obj2 = m.m_ObjManager->getTile(Course::Coordinate(0, 0));
    QVERIFY(obj2 != nullptr);

    m.removeItem(obj2);
    // Nothing should be removed
    QVERIFY(obj2 != nullptr);
}

void Test_Game::test_tile_update()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P1", 1));
    ds.players.push_back(std::make_pair("P2", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    m.import_data(ds);
    try {
        std::shared_ptr<Course::TileBase> obj2 = m.m_ObjManager->getTile(Course::Coordinate(0, 0));
        std::shared_ptr<Course::BuildingBase> building = std::make_shared<Student::Outpost>(m.m_GEHandler, m.m_ObjManager, m.m_ObjManager->getPlayer(0));
        std::shared_ptr<Course::WorkerBase> worker = std::make_shared<Student::Worker>(m.m_GEHandler, m.m_ObjManager, m.m_ObjManager->getPlayer(0));
        obj2->addBuilding(building);
        obj2->addWorker(worker);
        QVERIFY(obj2->getBuildingCount()>0);
        QVERIFY(obj2->getWorkerCount()>0);
    } catch (...) {
        QFAIL("Error in tile update");
    }
}

void Test_Game::test_unit_creation()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P1", 1));
    ds.players.push_back(std::make_pair("P2", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    m.import_data(ds);

    std::shared_ptr<Course::TileBase> obj = m.m_ObjManager->getTile(Course::Coordinate(1, 4));
    std::shared_ptr<Course::TileBase> obj2 = m.m_ObjManager->getTile(Course::Coordinate(2, 2));
    std::shared_ptr<Course::WorkerBase> worker = std::make_shared<Student::Worker>(m.m_GEHandler, m.m_ObjManager, m.m_ObjManager->getPlayer(0));
    std::shared_ptr<Course::WorkerBase> archer = std::make_shared<Student::Archer>(m.m_GEHandler, m.m_ObjManager, m.m_ObjManager->getPlayer(0));
    obj->addWorker(archer);
    obj2->addWorker(worker);
    QVERIFY(obj->getWorkerCount()>0);
    QVERIFY(obj2->getWorkerCount()>0);
    QVERIFY(obj->getWorkers()[0]->getType() == "Archer");
    QVERIFY(obj2->getWorkers()[0]->getType() == "Worker");
}

void Test_Game::building_creation()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P1", 1));
    ds.players.push_back(std::make_pair("P2", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    m.import_data(ds);

    try {

        std::shared_ptr<Course::TileBase> obj = m.m_ObjManager->getTile(Course::Coordinate(1, 4));
        std::shared_ptr<Course::TileBase> obj2 = m.m_ObjManager->getTile(Course::Coordinate(2, 2));
        std::shared_ptr<Course::BuildingBase> outpost = std::make_shared<Student::Outpost>(m.m_GEHandler, m.m_ObjManager, m.m_ObjManager->getPlayer(0));
        obj2->addBuilding(outpost);
        QVERIFY(obj2->getBuildingCount()>0);
        QVERIFY(obj2->getBuildings()[0]->getType() == "Outpost");

    } catch (...) {
        QFAIL("Object Creation failed due to an error.");
    }
}

void Test_Game::test_unit_movement()
{
    QSKIP("Test unit movement could be tested here, but it requires a lot of work.");
}

void Test_Game::test_battle_system()
{
    QSKIP("Test battle system could be tested here, but it requires a lot of work.");
}

void Test_Game::test_resources()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P1", 1));
    ds.players.push_back(std::make_pair("P2", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    m.import_data(ds);
    // This initializes 500 pieces of each resource to players 0 and 1.

    m.m_GEHandler->addToResourceMap(0, Student::ConstResourceMaps::INIT_RES);
    // Adds 500 pieces of each resource to player 0.

    for (unsigned int i = 0; i < 4; i++) {
        QVERIFY(m.m_GEHandler->getResources(0).at(i) == 1000);
        QVERIFY(m.m_GEHandler->getResources(1).at(i) == 500);
    }
    m.m_GEHandler->substractFromResourceMap(0, Student::ConstResourceMaps::INIT_RES);
    m.m_GEHandler->substractFromResourceMap(1, Student::ConstResourceMaps::INIT_RES);
    // Substract 500 pieces of each resource from players 0 and 1.

    for (unsigned int i = 0; i < 4; i++) {
        QVERIFY(m.m_GEHandler->getResources(0).at(i) == 500);
        QVERIFY(m.m_GEHandler->getResources(1).at(i) == 0);
    }

}

void Test_Game::test_game_initialization()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P1", 1));
    ds.players.push_back(std::make_pair("P2", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    m.import_data(ds);

    QCOMPARE(m.player_index_, 0u);
    QCOMPARE(m.turn_, 1u);
    m.on_endTurnB_clicked();
    QCOMPARE(m.player_index_, 1u);
    m.on_endTurnB_clicked();
    QCOMPARE(m.player_index_, 0u);
    QCOMPARE(m.turn_, 2u);

}

void Test_Game::test_updateTurnLabel()
{
    MapWindow m;
    data_struct ds;
    ds.players = {};
    ds.players.push_back(std::make_pair("P3", 1));
    ds.players.push_back(std::make_pair("P4", 1));
    ds.map_seed = 2;
    ds.chosen_preset = 1;
    //m.import_data(ds);

    QCOMPARE(m.player_index_, 0u);
    QCOMPARE(m.turn_, 1u);
    try {

        /*
        m.updateTurnLabel();
        QVERIFY(m.m_ui->turnLabel->text() == "Turn 1 of player: P3");
        m.on_endTurnB_clicked();
        QVERIFY(m.m_ui->turnLabel->text() == "Turn 1 of player: P4");
        m.on_endTurnB_clicked();
        QVERIFY(m.m_ui->turnLabel->text() == "Turn 2 of player: P3");
        */
    } catch (...) {
        QFAIL("Error in turn labels");
    }

}

//#include "tst_game_test.moc"
