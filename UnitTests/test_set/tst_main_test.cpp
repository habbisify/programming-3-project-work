#include <QtTest>
#include <QApplication>

#include "tst_course_test.h"
#include "tst_settings_test.h"
#include "tst_game_test.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    Test_Course ct;
    QTest::qExec(&ct);

    Test_Game gt;
    QTest::qExec(&gt);

    Test_Settings st;
    QTest::qExec(&st);

  return 0;

}
