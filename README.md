# Initial Timeline Plan

## Deadlines

- Codes available: 18.9
- Sprint 1 phase: 18.9 - 4.10
- Sprint 2 phase: 5.10 - 20.10
- Sprint 3 phase: 21.10 - 10.11
- Sprint 4 phase: 11.11 - 28.11
- Final submission: 29.11

---
## Sprint 1 content

### Minimum implementation

- Basic UI from the course side
- Game-mechanics implementation: collect resources, turn-based
- Start menu with settings (dialog window)
- Course Tile-classes: Grass, Forest
- Course Building-classes: HQ, Farm, "Outpost"
- Course Worker-class: Worker
- One own Tile-class: Mountain
- One own Building-class: Mine
- UnitTests for both interface -implementations: Start menu, Game window
- Final document

### Other tasks

- Get to know course interfaces
- CI - pipeline tests
- Work division for Sprint 2
- Adjustments to Sprint 2
- Plan for additional features

---
## Sprint 2 content

### Intermediate implementation

- Minimum implementation (Done in Sprint 1)
- Own graphics to Start Menu
- Own graphics to Game Window
- 3 own Tile-classes: Mountain, Field, Rock formation
- 2 own Building-classes: Mine, Sawmill
- 2 own Worker-classes: Spearman, Cavalry

### Other tasks

- Work division for Sprint 3
- Adjustments to Sprint 3
- Plan for additional features
- Exception handling for existing features

---
## Sprint 3 content

### Epic implementation

- Intermediate requirements (Done in previous Sprints)
- AI (0.5 - 1.0)
- Additional Tiles: Water
- Additional Buildings: Quarry
- Additional Workers: Archer (0.5)
- Nice graphics: Tiles, Unit markers, Building markers (0.5 - 1.0)
- Great GUI: User-friendly and intuitive (Menus and Game window) (0.5 - 1.0)
- Back story: Own window (button from Start menu) (0.5)
- New game mechanics: Randomly selected win condition (0.5 - 1.0), Timer (per turn)

### Other tasks

- Settings page (button from Start menu)
- Create rules of the game to unique window (button from Start menu)
- High quality documentation
- Extra unit tests for new features
- Work division for Sprint 4
- Adjustments for Sprint 4
- 3 own Tile-classes: Mountain, Field, Rock formation
- 2 own Building-classes: Mine, Quarry, Sawmill

---
## Sprint 4 content

### Tasks
- Fix all found bugs
- Play game
- Improve documentation
- Document proper work division
- Improved view for units and buildings
- Improved view for rules of the game
- Final document

### Final document
- Game-rules
- Game-controls
- Class responsibilities
- Division of labour.

---

# Actual realized timetable and division of work
We did a lot of planning early on, but the timing of the coding part can be seen on our GIT-repository. We didn't start late because of arrogance, but because we were very busy with one or two more simultaneus courses than we were able to handle. Fortunately, we were able to generate quite a lot of content on the overtime.

**Division of work can be best seen in our GIT repository. If the grader doesn't wan't to look ~150 commit messages through, then the general picture is given in WorkDivision.md. For a big picture on how our implementation is connected to to interfaces provided by the Course side, see class_responsibilities.jpg.**
***
***
# Game

![Logo](Game/imgs/logo_bigger.png "Logo")

## Story
Seven Deadly Sins have conquered Pirkanmaa. You can try to stop them. The Wrath, Pride, Greed and Sloth could kill us all... This is a last message you hear from me. Please, save us!

We provide you with a great army and basic buildings for you to defend yourself. The mighty Archers from Ylöjärvi, smart Spearmen from Kangasala, fast cavalry from Nokia and of course hard-working students (workers) from Tampere.

If you fail, there is no Tampere, no Pirkanmaa - It is just an awful unhabitable wasteland. Defend your HQ and remember "Attack is the best defense".

If you win the game, you could get lucky. Especially, if you rate this game 6/5, you will get a reward. With bad rating, the seven deadly sins are after you. Do you feel lucky, punk?

## Basic information (edited to correspond final solution)
The game consists of a game board, which is built from 10x10 tiles.
Each tile has one unit slot and one building slot.
2 - 4 players (containing 0 - 3 AI players, though these are normal players that are player-controllable)

![Player 1](Game/imgs/worker1.png "Player 1")
![Player 2](Game/imgs/worker2.png "Player 2")
![Player 3](Game/imgs/worker3.png "Player 3")
![Player 4](Game/imgs/worker4.png "Player 4")

There are multiple winning conditions - one is selected for every game 

1. Destroy all other players' HQs to win the game.
2. Control 15 tiles to win the game. (not implemented)

Player can construct beneficial buildings, which also provide cover from enemies.
1. HQ - Provides gold and food every turn and can create workers to adjacent squares
2. Outpost - Can create military units to adjacent squares
3. Farm - Doubles food production of its tile
4. Sawmill - Doubles wood production of its tile
5. Quarry - Doubles stone production of its tile
6. Mine - Doubles ore production of its tile

Player can train different unit types to control the game world and to kill enemy units and destroy enemy buildings.
1. Worker - Can collect resources and fight enemy units. Poor in combat.
2. Spearman - Can fight enemy units. Strong against cavalry. Excels at taking down heavily fortified targets - has great chances of success in an assault against HQ and Outpost, but lacks the mobility to raid improvement buildings.
3. Cavalry - Can fight enemy units. Strong against archer. Average chances of succesfull assault against all buildings.
4. Archer - Can fight enemy units. Strong against spearman. Excellent at raiding improvement buildings (mill, sawmill, mine, quarry), but can seldom take down heavily fortified targets (HQ and Outpost).

Player can move their units on the board and attack enemy units by moving on them. Ownership of units is shown visually - player 1 controls blue, p2 red, p3 yellow and p4 green units and buildings.
Buildings can be constructed only next to own existing buildings. Only one building can exist in a square. Buildings can't be constructed in water.
Workers can be trained to squares next to own HQs. Military units (spearman, cavalry, archer) can be only created next to own outposts. Units can't swim and therefore can't be placed on water. Only one unit can exist in a square.
Units and buildings cost resources indicated in the Info area once the unit or building is chosen from the shop. If the player doesn't have enough resources, nothing happens.
At the end of the turn, resources from different incomes will be added to the player's inventory. HQs generate resources passively, workers gather resources from the tiles they are standing on. Improvement buildings (mill, sawmill, mine, quarry) double the resource collection rate of the resource they enhance on the square.

---
## Settings-window
One can choose the amount of human players and bots and change their default names.
![Player 1](Game/imgs/worker1.png "Player 1, name defaults to 'Greed'")
![Player 2](Game/imgs/worker2.png "Player 2, 'Wrath'")
![Player 3](Game/imgs/worker3.png "Player 3, 'Pride'")
![Player 4](Game/imgs/worker4.png "Player 4, 'Sloth'")
Map seed can be selected. Note that if a random preset is chosen, a constant seed doesn't still provide a constant map.
There are 5 fixed presets and 1 totally random preset for tile prevalences.
1. Green hills is the default map. It features a nice variety of different landscapes with plenty of food available.
2. Arabia is a very open map, where control of all 4 different resources can prove very challenging. Recommended for serious strategists.
3. Forest Nothing features trees, trees and more trees. Remember to bring ya axe with you and chop away before Greenpeace finds out.
4. Mount Everest surely has an abundance of stone and ore, but finding good sources of food and wood may be difficult. No rewards for the player who tops the mountain first.
5. Kylmäkoski might sound familiar place. It embraces the glorious landscapes of Pirkanmaa - forests and fields.
6. Random is the perfect choice for players that like to spice it up. Beware that some map generations won't be too happy - you can always start the game again to achieve different setup.

Winning condition can be selected - either Destroy HQ or Conquer tiles.
The backstory of the game and game rules can be found on the corresponding tabs.

---
## First turn
Players start with a HQ and 1 worker stationed in it.
Player may make any legal actions during the first turn, as with any other turns.
Player presses "End turn" button and gets resources from workers tiles.
The GUI tells quite clearly, whose turn it is at any given time.

--- 
## Following turns
Units can be moved on the map.
- Movement can be made only to adjacent squares.
- We intended to restrict the movement of each unit for 1 tile / turn (Exception: Cavalry can move 2 tiles per turn).
- If the destination square does not have a unit, the movement always succeeds.
- If the destination square has an enemy unit or building, the units fight and the outcome follows the combat tables that tell the percentage odds for all match-ups (explained in more detail later).
- If the destination square has own stationed unit already, the unit does not move (nor spend "available movement").
- The destination square must not be a water tile.
- The Info area always tells if the movement succeed or not. If the movement failed, it also tells the reason why it failed.

Player can construct buildings.
- The construction square must be adjacent to an already existing own building.
- The player must have enough resources for the building.
- The square must have an empty building slot.
- The square must not have an enemy unit in its unit slot.
- The square must not be a water tile.
- The Info area always tells if the construction succeed or not. If the construction failed, it also tells the reason why it failed.

Player can train workers.
- The worker can only be trained to a square next to own HQ.
- The player must have enough resources to cover the costs of training a worker.
- The square must have an empty unit slot.
- The square must not be a water tile.

Player can train military units.
- The unit can only be trained to a square next to own outpost.
- The player must have enough resources to cover the costs of training the unit.
- The square must have an empty unit slot.
- The square must not be a water tile.

Player can end the turn, whenever they want.
- The resources are gathered from each of the tiles with a worker and from HQ, following the tables presented.
- Improvement buildings double the normal collection rate of a specific resource, provided that a worker is stationed in the square.
- The resources are added automatically to the turn-ending player's inventory.

---
## Resources

### Money 
![Money](Game/imgs/gold.png "Money")

#### How to get
| Way               | Amount (per turn) |
| ----------------- | ----------------- |
| HQ without worker | 50                |
| HQ with worker    | 150               |

HQ generates 50 money per turn.
HQ generates extra 100 (=150) money per turn, if there is own worker in player's HQ.

#### Usage
| Unit                  | Amount |
| --------------------- | ------ |
| [Spearman](#spearman) | 200    |
| [Cavalry](#cavalry)   | 300    |
| [Archer](#archer)     | 200    |

### Food 
![Food](Game/imgs/food.png "Food")

#### How to get
| Way                        | Amount (per turn) |
| -------------------------- | ----------------- |
| HQ without worker          | 10                |
| HQ with worker             | 50                |
| Grass with worker          | 50                |
| Field with worker          | 100               |
| Grass with worker and farm | 100               |
| Field with worker and farm | 200               |

#### Usage
| Unit                  | Amount |
| --------------------- | ------ |
| [Spearman](#spearman) | 300    |
| [Cavalry](#cavalry)   | 500    |
| [Archer](#archer)     | 300    |

### Wood 
![Wood](Game/imgs/wood.png "Wood")
#### How to get
| Way                            | Amount (per turn) |
| ------------------------------ | ----------------- |
| Grass with worker              | 25                |
| Forest with worker             | 100               |
| Grass with worker and sawmill  | 50                |
| Forest with worker and sawmill | 200               |

#### Usage
| Unit                  | Amount |
| --------------------- | ------ |
| [Spearman](#spearman) | 150    |
| [Cavalry](#cavalry)   | 100    |
| [Archer](#archer)     | 250    |

| Building              | Amount |
| --------------------- | ------ |
| [HQ](#hq)             | 1500   |
| [Outpost](#outpost)   | 600    |
| [Farm](#farm)         | 400    |
| [Mine](#mine)         | 100    |
| [Quarry](#quarry)     | 100    |

### Stone 
![Stone](Game/imgs/stone.png "Stone")

#### How to get
| Way                              | Amount (per turn) |
| -------------------------------- | ----------------- |
| Mountain with worker             | 25                |
| Rocks with worker                | 100               |
| Mountain with worker and quarry  | 50                |
| Rocks with worker and quarry     | 200               |

#### Usage
| Unit                  | Amount |
| --------------------- | ------ |
| [Spearman](#spearman) | 100    |
| [Archer](#archer)     | 100    |

| Building              | Amount |
| --------------------- | ------ |
| [HQ](#hq)             | 1500   |
| [Outpost](#outpost)   | 600    |
| [Farm](#farm)         | 100    |
| [Mine](#mine)         | 500    |
| [Sawmill](#sawmill)   | 300    |

### Ore 
![Ore](Game/imgs/ore.png "Ore")

#### How to get
| Way                           | Amount (per turn) |
| ----------------------------- | ----------------- |
| Rocks with worker             | 25                |
| Mountain with worker          | 100               |
| Rocks with worker and mine    | 50                |
| Mountain with worker and mine | 200               |

#### Usage
| Unit                  | Amount |
| --------------------- | ------ |
| [Cavalry](#cavalry)   | 200    |
| [Archer](#archer)     | 100    |

| Building              | Amount |
| --------------------- | ------ |
| [HQ](#hq)             | 1500   |
| [Outpost](#outpost)   | 600    |
| [Farm](#farm)         | 100    |
| [Quarry](#quarry)     | 500    |
| [Sawmill](#sawmill)   | 300    |

### Initial resources
| Resource | Amount |
| -------- | ------ |
| Money    | 500    |
| Food     | 500    |
| Wood     | 500    |
| Stone    | 500    |
| Ore      | 500    |

---
---
## Tiles

### Grass
![Grass](Game/imgs/grass.png "Grass")
Production per turn: 0,50,25,0,0
### Forest
![Forest](Game/imgs/forest.png "Forest")
Production per turn: 0,0,100,0,0
### Field
![Field](Game/imgs/field.png "Field")
Production per turn: 0,100,0,0,0
### Mountain
![Mountain](Game/imgs/mountain.png "Mountain")
Production per turn: 0,0,0,25,100
### Rocks
![Rocks](Game/imgs/rocks.png "Rocks")
Production per turn: 0,0,0,100,25
### Water
![Water](Game/imgs/water.png "Water")
Production per turn: 0,0,0,0,0
Units cannot go through water, so the map control is more important with maps containing water.

### Resource production
| Tile     | Money | Food | Wood | Stone | Ore |
| -------- | ----- | ---- | ---- | ----- | --- |
| Grass    | 0     | 50   | 25   | 0     | 0   |
| Forest   | 0     | 0    | 100  | 0     | 0   |
| Field    | 0     | 100  | 0    | 0     | 0   |
| Mountain | 0     | 0    | 0    | 25    | 100 |
| Rocks    | 0     | 0    | 0    | 100   | 25  |
| Water    | 0     | 0    | 0    | 0     | 0   |

---
## Buildings

### HQ
![HQ](Game/imgs/hq1.png "HQ")
Price: 0,0,1500,1500,1500
Produces money and a little bit food.
Player can create workers from HQ.
If player's all HQs are destroyed, the player loses the game.
### Outpost
![Outpost](Game/imgs/outpost1.png "Outpost")
Price: 0,0,600,600,600
Player can create military units (Spearman, Cavalry, Archer) from outpost.
### Farm
![Farm](Game/imgs/farm1.png "Farm")
Price:  0,0,400,100,100
Farm doubles the food production of the tile.
### Mine
![Mine](Game/imgs/mine1.png "Mine")
Price:  0,0,100,500,0
Mine doubles the ore production of the tile.
### Quarry
![Quarry](Game/imgs/quarry1.png "Quarry")
Price:  0,0,100,0,500
Quarry doubles the stone production of the tile.
### Sawmill
![Sawmill](Game/imgs/sawmill1.png "Sawmill")
Price:  0,0,0,300,300
Sawmill doubles the wood production of the tile.

### Initial buildings
| Building | Amount |
| -------- | ------ |
| HQ       | 1      |
| Outpost  | 0      |
| Farm     | 0      |
| Mine     | 0      |
| Quarry   | 0      |
| Sawmill  | 0      |

### Building costs

| Building | Money | Food | Wood | Stone | Ore  |
| -------- | ----- | ---- | ---- | ----- | ---- |
| HQ       | 0     | 0    | 2500 | 2500  | 2500 |
| Outpost  | 0     | 0    | 600  | 600   | 600  |
| Farm     | 0     | 0    | 400  | 100   | 100  |
| Mine     | 0     | 0    | 100  | 500   | 0    |
| Quarry   | 0     | 0    | 100  | 0     | 500  |
| Sawmill  | 0     | 0    | 0    | 300   | 300  |

---
## Units

### Worker
![Worker](Game/imgs/worker1.png "Worker")
Price: 0,300,0,0,0
### Spearman
![Spearman](Game/imgs/spearman1.png "Spearman")
Price: 200,300,150,100,0
### Cavalry
![Cavalry](Game/imgs/cavalry1.png "Cavalry")
Price: 300,500,100,0,200
### Archer
![Archer](Game/imgs/archer1.png "Archer")
Price: 200,300,250,100,100

### Initial units
| Unit     | Amount |
| -------- | ------ |
| Worker   | 1      |
| Spearman | 0      |
| Cavalry  | 0      |
| Archer   | 0      |

### Unit costs
| Unit     | Money | Food | Wood | Stone | Ore |
| -------- | ----- | ---- | ---- | ----- | --- |
| Worker   | 0     | 100  | 0    | 0     | 0   |
| Spearman | 200   | 300  | 150  | 100   | 0   |
| Cavalry  | 300   | 500  | 100  | 0     | 200 |
| Archer   | 200   | 300  | 250  | 100   | 100 |

---
## Fighting table

### Win percentage versus enemy units
| Versus   | Worker | Spearman | Cavalry | Archer |
| -------- | ------ | -------- | ------- | ------ |
| Worker   | 50%    | 97%      | 95%     | 98%    |
| Spearman | 3%     | 50%      | 25%     | 75%    |
| Cavalry  | 5%     | 75%      | 50%     | 25%    |
| Archer   | 2%     | 25%      | 75%     | 50%    |

### Win percentage versus enemy buildings
| Versus  | Worker | Spearman | Cavalry | Archer |
| ------- | ------ | -------- | ------- | ------ |
| HQ      | 1%     | 50%      | 30%     | 10%    |
| Outpost | 5%     | 80%      | 60%     | 30%    |
| Farm    | 10%    | 40%      | 60%     | 90%    |
| Mine    | 10%    | 40%      | 60%     | 90%    |
| Quarry  | 10%    | 40%      | 60%     | 90%    |
| Sawmill | 10%    | 40%      | 60%     | 90%    |

---

# Hints
- Investing heavily into economy early on is usually beneficial. However, don't be blind to opponent's potential rushing strategies!
- Try to secure yourself at least 1 tile of all different resource types on the map. You'll probably need all of them.
- Try to spend your resources rather than lose with them. You can't bring your resources to the afterlife.
- When defending, the best possible defence value is always used. Therefore you'll have better chance of defending heavily fortified buildings with archers and improvement buildings with cavalry, countering their counters.
- Though the movement isn't limited by the code, you should play as if it was to achieve balanced and fun gameplay. All units can only move one tile per turn, except for cavalry, which can move 2 tiles per turn.
- While playing this game, play some epic battle music on the background. This way you can achieve the correct atmosphere.

---

# Known issues
- Various bugs could occur in other setups than 2 player game (not exhausting testing performed to other player amounts)
- The GUI shows there is one extra player with 1 and 2 player settings. This is visual bug and doesn't affect game play
