# Division of labour

## Game mechanics

### Unit classes
- Worker: Joni
- Spearman: Joni
- Archer: Joni
- Cavalry: Joni

### Building classes
- HQ: Joni
- Outpost: Joni
- Farm: Joni
- Sawmill: Joni
- Mine: Joni
- Quarry: Joni

### Tile classes
- Forest (inherited): Matias
- Grassland (inherited): Matias
- Water: Joni
- Mountains: Joni
- Rocks: Joni
- Field: Joni

### Main window functionality
- Visualization effects: Matias
- Info messages: Joni
- Other parts divided equally: both

### Utilities
- All: Joni

### Main.cpp
- Signals, slots and handling of UIs: Matias

### GameEventHandler
- Battle mechanism: Matias
- Resource gathering: Joni
- Other own functionality: Matias

### ObjectManager
- Work divided equally: both

## UI
- Basic UI was planned together

### Settings Window
- Settings tab: Matias
- Story tab: Matias
- Game rules tab: Matias
- Graphics: Joni

### Game Window
- Map area: Course default
- Sidebar: Matias
- Graphics: Joni

### MapItem
- Basic things inherited from course
- Own drawing functionality: Matias

### GameScene
- Basic things inherited from course

### UnitTests
- All: Matias

### UML class diagram
- Based on diagram given in Plussa. Enhanced by Joni
